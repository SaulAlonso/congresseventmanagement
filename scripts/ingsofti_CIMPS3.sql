-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-09-2017 a las 12:23:30
-- Versión del servidor: 5.7.19-0ubuntu0.16.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ingsofti_CIMPS3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistentes_por_fecha`
--

DROP TABLE IF EXISTS `asistentes_por_fecha`;
CREATE TABLE `asistentes_por_fecha` (
  `id_user` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asistentes_por_fecha`
--

- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE `eventos` (
  `id` int(11) NOT NULL,
  `nombre_externo` text,
  `nombre_interno` text,
  `emision` year(4) NOT NULL,
  `descripcion` text,
  `id_titular` int(11) NOT NULL,
  `contacto` text,
  `requisitos` text NOT NULL,
  `id_tipo_evento` int(11) DEFAULT NULL,
  `id_idioma` int(11) DEFAULT NULL,
  `id_locacion` int(11) DEFAULT NULL,
  `inscripcion_apertura` datetime DEFAULT NULL,
  `inscripcion_cierre` datetime DEFAULT NULL,
  `cupo_max` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `foto` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Taba para el registro de talleres, ponencias y papers.';

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id`, `nombre_externo`, `nombre_interno`, `emision`, `descripcion`, `id_titular`, `contacto`, `requisitos`, `id_tipo_evento`, `id_idioma`, `id_locacion`, `inscripcion_apertura`, `inscripcion_cierre`, `cupo_max`, `fecha`, `estado`, `foto`) VALUES
(6, 'Programacion de microcontroladores', 'Programacion de microcontroladores', 2017, '', 1, 'Has-it', '', 2, 3, 3, '2017-10-02 00:00:00', '2017-10-17 00:00:00', 50, '2017-10-18', 1, 'img/wsi/21106617_758442811022825_2534446794587206618_n.jpg'),
(7, 'Practices for Addressing Environmental Sustainability through Requirements Processes. Analysis of environmental factors in the adoption of ISO/IEC 29110. Multiple case study.', 'Practices for Addressing Environmental Sustainability through Requirements Processes. Analysis of environmental factors in the adoption of ISO/IEC 29110. Multiple case study', 2017, '', 1, '', '', 1, 4, 3, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(8, 'DevSecOps+Risk Management', 'DevSecOps+Risk Management', 2017, '', 1, 'INEGI', '', 2, 3, 3, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-19', 1, NULL),
(9, 'Systematic Review: Cybersecurity Risk Taxonomy. A Theoretical Analysis of Digital Marketing Adoption by Startups.', 'Systematic Review: Cybersecurity Risk Taxonomy. A Theoretical Analysis of Digital Marketing Adoption by Startups', 2017, '', 1, '', '', 1, 3, 3, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(10, 'Integrated IT Governance and Management Model: Evaluation in a Developing Country', 'Integrated IT Governance and Management Model: Evaluation in a Developing Country', 2017, '', 1, '', '', 1, 3, 3, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(11, 'WYDIWYN – What You Define, Is What You Need: Defining Agile/Traditional Mixed Methodologies', 'WYDIWYN – What You Define, Is What You Need: Defining Agile/Traditional Mixed Methodologies', 2017, '', 1, '', '', 1, 3, 3, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(12, 'Decision-Support Platform for Industrial Recipe Management. Architecture for the integration of Linked Open Drug Data in an Augmented Reality application for mobile devices', 'Decision-Support Platform for Industrial Recipe Management. Architecture for the integration of Linked Open Drug Data in an Augmented Reality application for mobile devices', 2017, '', 1, '', '', 1, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(13, 'An Architecture based in Voice Command Recognition for faceted search in Linked Open Datasets.', 'An Architecture based in Voice Command Recognition for faceted search in Linked Open Datasets.', 2017, '', 1, '', '', 1, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(14, 'SmartLand-LD: A Linked Data approach for Integration of heterogeneous datasets to intelligent management of high biodiversity territories. Engineering Organizational Absorptive Capacity for Effective Knowledge Transfer', 'SmartLand-LD: A Linked Data approach for Integration of heterogeneous datasets to intelligent management of high biodiversity territories. Engineering Organizational Absorptive Capacity for Effective Knowledge Transfer', 2017, '', 1, '', '', 1, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(15, 'Introducción al desarrollo de exploits', 'Introducción al desarrollo de exploits', 2017, '', 1, 'M.C.C. Florencio J. González Rodríguez, Instituto Politecnico de Mexico', '', 2, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-18', 1, NULL),
(16, 'Introduccion al diseño digital con lenguajes de descripcion de hardware', 'Introduccion al diseño digital con lenguajes de descripcion de hardware', 2017, '', 1, 'Dr. Juan J. Raygoza Panduro, Universidad de Guadalajara', '', 2, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-19', 1, NULL),
(17, 'Optimización de Circuitos', 'Optimización de Circuitos', 2017, '', 1, ' CUCEI, Universidad de Guadalajara', '', 2, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-19', 1, NULL),
(18, 'Simulation and path planning for quadcopter obstacle avoidance in indoor environments using the ROS framework', 'Simulation and path planning for quadcopter obstacle avoidance in indoor environments using the ROS framework', 2017, '', 1, '', '', 1, 3, 4, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(19, 'Big Data visualization: Review of techniques and datasets. A Model Driven Method for Data migration', 'Big Data visualization: Review of techniques and datasets. A Model Driven Method for Data migration', 2017, '', 1, '', '', 1, 3, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(20, 'Mejora del Proceso para Comunicación de las Tareas de Estudiantes de Primaria entre Docentes y Padres de Familia', 'Mejora del Proceso para Comunicación de las Tareas de Estudiantes de Primaria entre Docentes y Padres de Familia', 2017, '', 1, '', '', 1, 3, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(21, 'Proposal for Identifying Teamwork Roles in Software Engineering through the Construction of a Virtual Rube Goldberg Machine. HiSPI: Tool to initiate software process improvement through a formal method', 'Proposal for Identifying Teamwork Roles in Software Engineering through the Construction of a Virtual Rube Goldberg Machine. HiSPI: Tool to initiate software process improvement through a formal method', 2017, '', 1, '', '', 1, 3, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(22, 'kanban game', 'kanban game', 2017, '', 1, 'CIMAT Unidad Zacatecas', '', 2, 3, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 24, '2017-10-18', 1, NULL),
(23, 'Mahi: Support Tool for Practicing First-Degree Alebraic Equations. Reinforcing DevOps approach with security and risk management: an experience of implementing it in a Data Center of a Mexican Organization', 'Mahi: Support Tool for Practicing First-Degree Alebraic Equations. Reinforcing DevOps approach with security and risk management: an experience of implementing it in a Data Center of a Mexican Organization', 2017, '', 1, '', '', 1, 4, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(24, 'Methodology to transform a monolithic software into a microservice architecture. Usability evaluation of collaborative applications with multimodal user interface. ', 'Methodology to transform a monolithic software into a microservice architecture. Usability evaluation of collaborative applications with multimodal user interface. ', 2017, '', 1, '', '', 1, 4, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(25, 'Evaluation of Design and Code Revisions in Academic Practices of Software Engineering. A Semantic Web Application in Business Data Integration.', 'Evaluation of Design and Code Revisions in Academic Practices of Software Engineering. A Semantic Web Application in Business Data Integration.', 2017, '', 1, '', '', 1, 4, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(26, 'Optimización de un modelo de carga masiva de datos mediante motores de búsqueda para sitios web orientados al Comercio Electrónico. Proposal of Methodology for a Data WareHousing Process. Lifecycle Coverage Analysis via Multi-Agent System Methodology.', 'Optimización de un modelo de carga masiva de datos mediante motores de búsqueda para sitios web orientados al Comercio Electrónico. Proposal of Methodology for a Data WareHousing Process. Lifecycle Coverage Analysis via Multi-Agent System Methodology.', 2017, '', 1, '', '', 1, 3, 5, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(28, 'ROBOT COMPETICION ISO 29110', 'ROBOT COMPETICION ISO 29110', 2017, '', 1, 'NOMADA', 'Portátil con ambiente windows', 2, 4, 6, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-18', 1, NULL),
(29, 'Introduccion a Unity', 'Introduccion a Unity', 2010, '', 1, 'CIMAT Unidad Zacatecas', 'Portátil con ambiente windows, linux.', 2, 3, 6, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-19', 1, NULL),
(30, 'A Means-Ends Design of SCRUM+: an agile-disciplined balanced SCRUM enhanced with the ISO/IEC 29110 Standard. Project Portfolio Management in Small Context in Software Industry: A Systematic Literature Review', 'A Means-Ends Design of SCRUM+: an agile-disciplined balanced SCRUM enhanced with the ISO/IEC 29110 Standard. Project Portfolio Management in Small Context in Software Industry: A Systematic Literature Review', 2017, '', 1, '', '', 1, 4, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(31, 'Multiple Software Product Lines: applications and challenges.', 'Multiple Software Product Lines: applications and challenges.', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(32, 'Automated software generation process with SPL. ISO/IEC 29110 and curricula programs related to Computer Science and Informatics in Mexico: Analysis of practices coverage', 'Automated software generation process with SPL. ISO/IEC 29110 and curricula programs related to Computer Science and Informatics in Mexico: Analysis of practices coverage', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(33, 'Dr. Tomas San-Feliu Gilabert. Facultad de Informatica. Universidad Politecnica de Madrid, España', 'Dr. Tomas San-Feliu Gilabert. Facultad de Informatica. Universidad Politecnica de Madrid, España', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(34, 'Ing./MFN Gabriel Suárez, IEEE Aguascalientes Section Chair: Liderazgo para alto desempeño profesional', 'Ing./MFN Gabriel Suárez, IEEE Aguascalientes Section Chair: Liderazgo para alto desempeño profesional', 2017, '', 1, 'IEEE', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-18', 1, NULL),
(35, 'Tips para solicitar empleos: Oficina de empleo', 'Tips para solicitar empleos: Oficina de empleo', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(36, 'Financial impact on the adoption of software validation tasks in the analysis phase: A business case.', 'Financial impact on the adoption of software validation tasks in the analysis phase: A business case.', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(37, 'Soft Skills for IT Project Success: A Systematic Literature Review. Formalizing a Cost Construct Model related to the Software Requirements Elicitation Techniques', 'Soft Skills for IT Project Success: A Systematic Literature Review. Formalizing a Cost Construct Model related to the Software Requirements Elicitation Techniques', 2017, '', 1, '', '', 1, 4, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(38, 'Pasa de emprendedor a empresario', 'Pasa de emprendedor a empresario', 2017, '', 1, 'Ulises Mejia Haro', '', 2, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(39, 'Jorge Leon. Guanajuato', 'Jorge Leon. Guanajuato', 2017, '', 1, 'Jorge Leon', '', 2, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(40, 'Curso fondos INADEM: Secretaria de Economia: Estudiantes, 100', 'Curso fondos INADEM: Secretaria de Economia: Estudiantes, 100', 2017, '', 1, 'INADEM, Secretaria de Economia', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(41, 'IEEE Woman in Engineering (WIE)', 'IEEE Woman in Engineering (WIE)', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(42, 'Actividades de las Ramas Estudiantiles Sección Guadalajara. IEEE Guadalajara Section Chair', 'Actividades de las Ramas Estudiantiles Sección Guadalajara. IEEE Guadalajara Section Chair', 2017, '', 1, '', '', 1, 3, 9, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(43, 'Introducción a Blender', 'Introducción a Blender', 2017, '', 833, 'Dr. Adriana Peña Pérez Negrón, Dr. Graciela Lara Lopez. CUCEI, Universidad de Guadalajara', '', 2, 3, 7, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-18', 1, NULL),
(44, 'Introducción al Diseño de videojuegos con unreal engine', 'Introducción al Diseño de videojuegos con unreal engine', 2017, '', 1, 'M. José Luis David Bonilla Carranza. Universidad de Guadalajara', 'Windows 7/8 64-bit  o MAC, Processor, Quad-core Intel or AMD, 2.5 GHz or faster, Memory,  8 GB RAM, Video,  Card/DirectX Version,  DirectX 11 compatible graphics card.', 2, 3, 7, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 50, '2017-10-19', 1, NULL),
(45, 'Introducción a la Configuración NAO', 'Introducción a la Configuración NAO', 2017, '', 849, 'Facultad de Informatica, Universidad Autonoma de Sinaloa', '', 2, 3, 8, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 49, '2017-10-18', 1, NULL),
(46, 'Gestión de Proyectos', 'Gestión de Proyectos', 2017, '', 1, 'Luis Castrejón, Coach PMP', '', 2, 3, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 100, '2017-10-20', 1, NULL),
(47, 'Towards Detecting MVC Architectural Smells. 3D objects\' shape relevance for saliency measure.', 'Towards Detecting MVC Architectural Smells. 3D objects\' shape relevance for saliency measure.', 2017, '', 1, '', '', 1, 3, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(48, 'Software testing education through a collaborative virtual approach.', 'Software testing education through a collaborative virtual approach.', 2017, '', 1, '', '', 1, 3, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(49, 'Convocatoria de Proyectos. Secretaria de Economia, Gobierno de Zacatecas. Reunion de empresas estatales', 'Convocatoria de Proyectos. Secretaria de Economia, Gobierno de Zacatecas. Reunion de empresas estatales', 2017, '', 1, 'Laura Ruelas, Secretaria de Economia, Gobierno de Zacatecas', '', 1, 3, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-19', 1, NULL),
(50, ' Fondos H2020. Dr. Alvaro Rocha. Reunion empresas, docentes, investigadores, persoanlidades de gobierno.', ' Fondos H2020. Dr. Alvaro Rocha. Reunion empresas, docentes, investigadores, persoanlidades de gobierno.', 2017, '', 1, 'Dr. Alvaro Rocha', '', 1, 3, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(51, 'ISO291110 Next steps. Dr. Claude Laporte 200 personas, Empresas de éxito, Robot competicion', 'ISO291110 Next steps. Dr. Claude Laporte 200 personas, Empresas de éxito, Robot competicion', 2017, '', 1, 'Dr. Claude Laporte', '', 1, 4, 10, '2017-10-02 00:00:00', '2017-10-16 00:00:00', 0, '2017-10-20', 1, NULL),
(52, 'Dr. Gonzalo Cuevas Agustin. Universidad Politecnica de Madrid, España.', 'Dr. Gonzalo Cuevas Agustin. Universidad Politecnica de Madrid, España.', 2017, '', 1, 'Dr. Gonzalo Cuevas Agustin', '', 1, 4, 9, '2017-10-02 00:00:00', '2017-10-02 00:00:00', 0, '2017-10-18', 1, NULL),
(53, 'Víctor A. Bustos, Dirección General Adjunta de Investigación , INEGI', 'Víctor A. Bustos, Dirección General Adjunta de Investigación , INEGI', 2017, '', 1, 'Víctor A. Bustos', '', 1, 3, 9, '2017-10-01 00:00:00', '2017-10-15 00:00:00', 0, '2017-10-18', 1, NULL),
(54, 'Dr. Alvaro Rocha. Universidad  de Coimbra, Portugal', 'Dr. Alvaro Rocha. Universidad  de Coimbra, Portugal', 2017, '', 1, 'Dr. Alvaro Rocha', '', 1, 3, 9, NULL, NULL, 0, '2017-10-18', 1, NULL),
(55, 'Ph.D. Stewart R. Santos Arce. IEEE Guadalajara Section Chair: Inducción a la Investigación Científica Oportunidades en el IEEE.', 'Ph.D. Stewart R. Santos Arce. IEEE Guadalajara Section Chair: Inducción a la Investigación Científica Oportunidades en el IEEE.', 2017, '', 1, 'Ph.D. Stewart R. Santos Arce', '', 1, 4, 9, NULL, NULL, 0, '2017-10-19', 1, NULL),
(56, 'Jorge Leon Guanajuato/Ulises Mejia Haro: Espiritu emprendedor. Café Punta del Cielo, Zacatecas.', 'Jorge Leon Guanajuato/Ulises Mejia Haro: Espiritu emprendedor. Café Punta del Cielo, Zacatecas.', 2017, '', 1, 'Jorge Leon Guanajuato/Ulises Mejia Haro', '', 1, 3, 9, NULL, NULL, 0, '2017-10-19', 1, NULL),
(57, 'Dr. Manuel Perez Cota, Universidad de Vigo, España', 'Dr. Manuel Perez Cota, Universidad de Vigo, España', 2017, '', 1, 'Dr. Manuel Perez Cota', '', 1, 3, 9, NULL, NULL, 0, '2017-10-19', 1, NULL),
(58, 'Cena Hotel Don Miguel: Autoridades, Ponentes, autores.', 'Cena Hotel Don Miguel: Autoridades, Ponentes, autores.', 2017, '', 1, '', '', 1, 4, 9, NULL, NULL, 0, '2017-10-19', 1, NULL),
(59, 'Dr. Claude Laporte, Coordinator Chair ISO29110 : The Development of Software with ISO/IEC 29110 Standards and Guides to Improve the Competitiveness of Enterprises in Quality, Cost and Schedule', 'Dr. Claude Laporte, Coordinator Chair ISO29110 : The Development of Software with ISO/IEC 29110 Standards and Guides to Improve the Competitiveness of Enterprises in Quality, Cost and Schedule', 2010, '', 1, 'Dr. Claude Laporte', '', 1, 4, 9, NULL, NULL, 0, '2017-10-20', 1, NULL),
(60, 'Juan Muñoz López, Director de Planeación y Normatividad Informática-INEGI', 'Juan Muñoz López, Director de Planeación y Normatividad Informática-INEGI', 2017, '', 1, 'Juan Muñoz López', '', 1, 3, 9, NULL, NULL, 0, '2017-10-20', 1, NULL),
(61, 'Auditorio. Clausura/mesa de Conclusiones academicas y empresariales ', 'Auditorio. Clausura/mesa de Conclusiones academicas y empresariales ', 2017, '', 1, '', '', 1, 3, 9, NULL, NULL, 0, '2017-10-20', 1, NULL),
(62, 'Usability analysis: Is our software inclusive? Impact of organizational and user factors on the acceptance and use of project management software in the medium-sized company in Lima', 'Usability analysis: Is our software inclusive? Impact of organizational and user factors on the acceptance and use of project management software in the medium-sized company in Lima', 2017, '', 1, '', '', 1, 4, 10, '2017-10-01 00:00:00', '2017-10-15 00:00:00', 0, '2017-10-19', 1, NULL),
(63, 'Introduccion al Diseño de videojuegos Colaborativos con phaser nodejs', 'Introduccion al Diseño de videojuegos Colaborativos con phaser nodejs', 2017, '', 1, 'Universidad de Guadalajara', 'Windows 7/8 64-bit ,  MAC, linux, Processor, Quad-core Intel or AMD, 2.5 GHz or faster, Memory,  2 GB RAM, Video.', 2, 3, 7, '2017-10-01 00:00:00', '2017-10-15 00:00:00', 50, '2017-10-19', 1, NULL),
(64, 'Analisis de datos para contar una historia', 'Analisis de datos para contar una historia.', 2017, '', 1, 'MIS. Freddy Iñiguez Lopez. Cimat Unidad Zacatecas.', '', 2, 3, 6, '2017-10-01 00:00:00', '2017-10-15 00:00:00', 0, '2017-10-20', 1, NULL),
(65, 'A Lean mind-set on the Information Technologies sector: Targeting and addressing waste for an increased performance.  A Brief Review on the Use of Sentiment Analysis Approaches in Social Networks', 'A Lean mind-set on the Information Technologies sector: Targeting and addressing waste for an increased performance.  A Brief Review on the Use of Sentiment Analysis Approaches in Social Networks', 2017, '', 1, '', '', 1, 4, 4, '2017-10-01 00:00:00', '2017-10-15 00:00:00', 0, '2017-10-20', 1, NULL),
(66, 'uuu', 'uuu', 2017, '', 904, '', '', 1, 4, 9, NULL, NULL, 9, NULL, 0, ''),
(67, 'ppp', 'ppp', 2017, '', 904, '', '', 1, 4, 9, NULL, NULL, 0, '2017-01-01', 0, 'img/wsi/21617602_723379234520471_3227360552675937268_n.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechas_por_evento`
--

DROP TABLE IF EXISTS `fechas_por_evento`;
CREATE TABLE `fechas_por_evento` (
  `id` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `estado` int(11) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fechas_por_evento`
--

INSERT INTO `fechas_por_evento` (`id`, `id_evento`, `fecha`, `hora_inicio`, `hora_fin`, `estado`, `descripcion`) VALUES
(13, 6, '2017-10-18', '10:00:00', '11:00:00', 1, ''),
(14, 6, '2017-10-18', '11:30:00', '13:00:00', 1, ''),
(15, 6, '2017-10-18', '15:30:00', '17:30:00', 1, ''),
(16, 7, '2017-10-19', '10:00:00', '11:00:00', 1, ''),
(17, 8, '2017-10-19', '11:30:00', '12:00:00', 1, ''),
(18, 52, '2017-10-18', '09:00:00', '10:00:00', 1, ''),
(19, 53, '2017-10-18', '13:00:00', '14:00:00', 1, ''),
(20, 54, '2017-10-18', '17:30:00', '18:30:00', 1, ''),
(21, 13, '2017-10-18', '11:30:00', '12:00:00', 1, ''),
(22, 14, '2017-10-18', '12:00:00', '13:00:00', 1, ''),
(23, 15, '2017-10-18', '15:30:00', '17:30:00', 1, ''),
(24, 19, '2017-10-18', '10:00:00', '11:00:00', 1, ''),
(25, 20, '2017-10-18', '11:30:00', '12:00:00', 1, ''),
(26, 21, '2017-10-18', '12:00:00', '13:00:00', 1, ''),
(27, 22, '2017-10-18', '15:30:00', '17:30:00', 1, ''),
(28, 28, '2017-10-18', '10:00:00', '11:00:00', 1, ''),
(29, 62, '2017-10-19', '12:00:00', '13:00:00', 1, ''),
(30, 63, '2017-10-19', '16:30:00', '17:30:00', 1, ''),
(31, 63, '2017-10-19', '10:00:00', '11:00:00', 1, ''),
(32, 63, '2017-10-19', '11:30:00', '13:00:00', 1, ''),
(33, 64, '2017-10-20', '11:30:00', '13:00:00', 1, ''),
(34, 65, '2017-10-20', '12:00:00', '13:00:00', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
CREATE TABLE `idiomas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `idiomas`
--

INSERT INTO `idiomas` (`id`, `nombre`) VALUES
(3, 'Español'),
(4, 'English');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locaciones`
--

DROP TABLE IF EXISTS `locaciones`;
CREATE TABLE `locaciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `cupo_maximo` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `descripcion` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `locaciones`
--

INSERT INTO `locaciones` (`id`, `nombre`, `cupo_maximo`, `direccion`, `lugar`, `descripcion`) VALUES
(3, 'Salón 1', 50, '', '', ''),
(4, 'Salón 2', 50, '', '', ''),
(5, 'Salón 3', 24, '', '', ''),
(6, 'Salón 4', 50, '', '', ''),
(7, 'Salón 5', 50, '', '', ''),
(8, 'Salón 6', 50, '', '', ''),
(9, 'Auditorio', 0, '', '', ''),
(10, 'Centro de negocios', 100, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_de_eventos`
--

DROP TABLE IF EXISTS `tipos_de_eventos`;
CREATE TABLE `tipos_de_eventos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipos_de_eventos`
--

INSERT INTO `tipos_de_eventos` (`id`, `nombre`) VALUES
(1, 'Ponencia'),
(2, 'Taller');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asistentes_por_fecha`
--
ALTER TABLE `asistentes_por_fecha`
  ADD PRIMARY KEY (`id_user`,`id_evento`),
  ADD KEY `id_fecha` (`id_evento`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `eventos`
--
ALTER TABLE `eventos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tipo_evento` (`id_tipo_evento`),
  ADD KEY `id_idioma` (`id_idioma`),
  ADD KEY `id_lugar` (`id_locacion`),
  ADD KEY `id_tipo_evento_2` (`id_tipo_evento`),
  ADD KEY `id_titular` (`id_titular`);

--
-- Indices de la tabla `fechas_por_evento`
--
ALTER TABLE `fechas_por_evento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_evento` (`id_evento`),
  ADD KEY `id_evento_2` (`id_evento`);

--
-- Indices de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `locaciones`
--
ALTER TABLE `locaciones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipos_de_eventos`
--
ALTER TABLE `tipos_de_eventos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `eventos`
--
ALTER TABLE `eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT de la tabla `fechas_por_evento`
--
ALTER TABLE `fechas_por_evento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `idiomas`
--
ALTER TABLE `idiomas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `locaciones`
--
ALTER TABLE `locaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `tipos_de_eventos`
--
ALTER TABLE `tipos_de_eventos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
