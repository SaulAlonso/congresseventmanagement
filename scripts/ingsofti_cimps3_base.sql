-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2017 a las 06:07:36
-- Versión del servidor: 5.7.20
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: ingsofti_CIMPS3
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla asistentes_por_fecha
--

CREATE TABLE asistentes_por_fecha (
  id_user int(11) NOT NULL,
  id_evento int(11) NOT NULL,
  asistencia tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla downloads
--

CREATE TABLE downloads (
  id int(11) NOT NULL,
  nombre varchar(500) NOT NULL,
  tipo varchar(200) NOT NULL,
  `group` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla eventos
--

CREATE TABLE eventos (
  id int(11) NOT NULL,
  nombre_externo text,
  nombre_interno text,
  emision year(4) NOT NULL,
  descripcion text,
  id_titular int(11) NOT NULL,
  contacto text,
  requisitos text NOT NULL,
  id_tipo_evento int(11) DEFAULT NULL,
  id_idioma int(11) DEFAULT NULL,
  id_locacion int(11) DEFAULT NULL,
  inscripcion_apertura datetime DEFAULT NULL,
  inscripcion_cierre datetime DEFAULT NULL,
  cupo_max int(11) DEFAULT NULL,
  fecha date DEFAULT NULL,
  estado tinyint(1) NOT NULL DEFAULT '1',
  foto text,
  confirmado tinyint(1) DEFAULT '0',
  moderador varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Taba para el registro de talleres, ponencias y papers.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla fechas_por_evento
--

CREATE TABLE fechas_por_evento (
  id int(11) NOT NULL,
  id_evento int(11) NOT NULL,
  fecha date NOT NULL,
  id_horario int(11) NOT NULL,
  hora_inicio time DEFAULT NULL,
  hora_fin time DEFAULT NULL,
  estado int(11) DEFAULT NULL,
  descripcion text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla groups
--

CREATE TABLE groups (
  id mediumint(8) UNSIGNED NOT NULL,
  name varchar(50) NOT NULL,
  description varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla groups_has_services
--

CREATE TABLE groups_has_services (
  groups_id mediumint(8) UNSIGNED NOT NULL,
  services_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla horarios_por_emision
--

CREATE TABLE horarios_por_emision (
  id int(11) NOT NULL,
  hora_inicio time NOT NULL,
  hora_fin time NOT NULL,
  emision int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contiene los horarios disponibles de cada emisión.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla idiomas
--

CREATE TABLE idiomas (
  id int(11) NOT NULL,
  nombre varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla locaciones
--

CREATE TABLE locaciones (
  id int(11) NOT NULL,
  emision int(11) NOT NULL,
  nombre varchar(45) NOT NULL,
  cupo_maximo int(11) NOT NULL,
  direccion varchar(100) DEFAULT NULL,
  lugar varchar(100) DEFAULT NULL,
  descripcion text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla login_attempts
--

CREATE TABLE login_attempts (
  id int(11) UNSIGNED NOT NULL,
  ip_address varbinary(16) NOT NULL,
  login varchar(100) NOT NULL,
  time int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla lugar
--

CREATE TABLE lugar (
  id int(1) NOT NULL,
  nombre varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  id int(11) NOT NULL,
  type_payment varchar(45) DEFAULT NULL,
  image varchar(45) DEFAULT NULL,
  bank varchar(45) DEFAULT NULL,
  reference varchar(255) DEFAULT NULL,
  tax_number varchar(45) DEFAULT NULL,
  users_id int(11) UNSIGNED NOT NULL,
  date date NOT NULL,
  organization varchar(500) NOT NULL,
  adress varchar(600) NOT NULL,
  locality varchar(100) NOT NULL,
  postal_code varchar(45) NOT NULL,
  country varchar(100) NOT NULL,
  phone_number varchar(45) NOT NULL,
  accepted tinyint(4) DEFAULT '0',
  discount double NOT NULL DEFAULT '0',
  discount_euros double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla programa
--

CREATE TABLE programa (
  id int(11) NOT NULL,
  nombre varchar(300) NOT NULL,
  dia date NOT NULL,
  hora_inicio time NOT NULL,
  hora_fin time NOT NULL,
  descripcion varchar(8000) NOT NULL,
  id_lugar int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla recovery
--

CREATE TABLE recovery (
  id tinyint(3) UNSIGNED NOT NULL,
  name varchar(300) NOT NULL,
  email varchar(500) NOT NULL,
  institute varchar(500) NOT NULL,
  `group` varchar(10) NOT NULL,
  total varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla services
--

CREATE TABLE services (
  id int(11) NOT NULL,
  name varchar(45) DEFAULT NULL,
  cost float DEFAULT NULL,
  euro float DEFAULT NULL,
  onlyone tinyint(4) DEFAULT '0',
  marked tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tbl_logs
--

CREATE TABLE tbl_logs (
  id int(11) NOT NULL,
  username varchar(50) DEFAULT NULL,
  ipaddress varchar(50) DEFAULT NULL,
  logtime datetime DEFAULT NULL,
  controller varchar(255) DEFAULT '',
  action text,
  details text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla tipos_de_eventos
--

CREATE TABLE tipos_de_eventos (
  id int(11) NOT NULL,
  nombre varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla users
--

CREATE TABLE users (
  id int(11) UNSIGNED NOT NULL,
  ip_address varbinary(16) NOT NULL,
  username varchar(100) NOT NULL,
  password varchar(80) NOT NULL,
  salt varchar(40) DEFAULT NULL,
  email varchar(100) NOT NULL,
  activation_code varchar(40) DEFAULT NULL,
  forgotten_password_code varchar(40) DEFAULT NULL,
  forgotten_password_time int(11) UNSIGNED DEFAULT NULL,
  remember_code varchar(40) DEFAULT NULL,
  created_on int(11) UNSIGNED NOT NULL,
  last_login int(11) UNSIGNED DEFAULT NULL,
  active tinyint(1) UNSIGNED DEFAULT NULL,
  name varchar(100) DEFAULT NULL,
  city varchar(50) DEFAULT NULL,
  country varchar(100) DEFAULT NULL,
  afiliation_name varchar(300) DEFAULT NULL,
  afiliation_adress varchar(500) DEFAULT NULL,
  gaffete tinyint(4) DEFAULT '0',
  tittle varchar(5) DEFAULT NULL,
  paper_id1 int(11) DEFAULT NULL,
  paper_id2 int(11) DEFAULT NULL,
  title1 varchar(500) DEFAULT NULL,
  title2 varchar(500) DEFAULT NULL,
  gender varchar(10) NOT NULL,
  accept tinyint(4) NOT NULL,
  control_num varchar(12) DEFAULT NULL COMMENT 'Número de control',
  reg_venue int(1) NOT NULL DEFAULT '1' COMMENT 'Sede',
  shirt_size varchar(3) DEFAULT NULL COMMENT 'Talla camisa',
  access_code varchar(25) DEFAULT NULL,
  registered tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla users_groups
--

CREATE TABLE users_groups (
  id int(11) UNSIGNED NOT NULL,
  user_id int(11) UNSIGNED NOT NULL,
  group_id mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla users_has_services
--

CREATE TABLE users_has_services (
  id int(11) NOT NULL,
  services_id int(11) DEFAULT NULL,
  quantity varchar(45) DEFAULT NULL,
  total float DEFAULT NULL,
  order_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla asistentes_por_fecha
--
ALTER TABLE asistentes_por_fecha
  ADD PRIMARY KEY (id_user,id_evento),
  ADD KEY id_fecha (id_evento),
  ADD KEY id_user (id_user);

--
-- Indices de la tabla downloads
--
ALTER TABLE downloads
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla eventos
--
ALTER TABLE eventos
  ADD PRIMARY KEY (id),
  ADD KEY id_tipo_evento (id_tipo_evento),
  ADD KEY id_idioma (id_idioma),
  ADD KEY id_lugar (id_locacion),
  ADD KEY id_tipo_evento_2 (id_tipo_evento),
  ADD KEY id_titular (id_titular);

--
-- Indices de la tabla fechas_por_evento
--
ALTER TABLE fechas_por_evento
  ADD PRIMARY KEY (id),
  ADD KEY id_evento (id_evento),
  ADD KEY id_evento_2 (id_evento),
  ADD KEY id_horario (id_horario);

--
-- Indices de la tabla groups
--
ALTER TABLE groups
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla groups_has_services
--
ALTER TABLE groups_has_services
  ADD PRIMARY KEY (groups_id,services_id),
  ADD KEY fk_groups_has_services_services1_idx (services_id),
  ADD KEY fk_groups_has_services_groups1_idx (groups_id);

--
-- Indices de la tabla horarios_por_emision
--
ALTER TABLE horarios_por_emision
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla idiomas
--
ALTER TABLE idiomas
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla locaciones
--
ALTER TABLE locaciones
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla login_attempts
--
ALTER TABLE login_attempts
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla lugar
--
ALTER TABLE lugar
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (id),
  ADD KEY fk_order_users1_idx (users_id);

--
-- Indices de la tabla programa
--
ALTER TABLE programa
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla recovery
--
ALTER TABLE recovery
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla services
--
ALTER TABLE services
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY name_UNIQUE (name);

--
-- Indices de la tabla tbl_logs
--
ALTER TABLE tbl_logs
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla tipos_de_eventos
--
ALTER TABLE tipos_de_eventos
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla users
--
ALTER TABLE users
  ADD PRIMARY KEY (id);

--
-- Indices de la tabla users_groups
--
ALTER TABLE users_groups
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY uc_users_groups (user_id,group_id),
  ADD KEY fk_users_groups_users1_idx (user_id),
  ADD KEY fk_users_groups_groups1_idx (group_id);

--
-- Indices de la tabla users_has_services
--
ALTER TABLE users_has_services
  ADD PRIMARY KEY (id),
  ADD KEY fk_users_has_services_services1_idx (services_id),
  ADD KEY fk_users_has_services_order1_idx (order_id);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla downloads
--
ALTER TABLE downloads
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla eventos
--
ALTER TABLE eventos
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT de la tabla fechas_por_evento
--
ALTER TABLE fechas_por_evento
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT de la tabla groups
--
ALTER TABLE groups
  MODIFY id mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla horarios_por_emision
--
ALTER TABLE horarios_por_emision
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla idiomas
--
ALTER TABLE idiomas
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla locaciones
--
ALTER TABLE locaciones
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla login_attempts
--
ALTER TABLE login_attempts
  MODIFY id int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla lugar
--
ALTER TABLE lugar
  MODIFY id int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1525;

--
-- AUTO_INCREMENT de la tabla programa
--
ALTER TABLE programa
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT de la tabla recovery
--
ALTER TABLE recovery
  MODIFY id tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT de la tabla services
--
ALTER TABLE services
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla tbl_logs
--
ALTER TABLE tbl_logs
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34436;

--
-- AUTO_INCREMENT de la tabla tipos_de_eventos
--
ALTER TABLE tipos_de_eventos
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla users
--
ALTER TABLE users
  MODIFY id int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1337;

--
-- AUTO_INCREMENT de la tabla users_groups
--
ALTER TABLE users_groups
  MODIFY id int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1782;

--
-- AUTO_INCREMENT de la tabla users_has_services
--
ALTER TABLE users_has_services
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1726;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
