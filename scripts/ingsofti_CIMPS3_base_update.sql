-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-12-2017 a las 04:53:07
-- Versión del servidor: 5.7.20
-- Versión de PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ingsofti_CIMPS3`
--
CREATE DATABASE IF NOT EXISTS `ingsofti_CIMPS3` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ingsofti_CIMPS3`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistentes_por_fecha`
--

DROP TABLE IF EXISTS `asistentes_por_fecha`;
CREATE TABLE IF NOT EXISTS `asistentes_por_fecha` (
  `id_user` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `asistencia` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_user`,`id_evento`),
  KEY `id_fecha` (`id_evento`),
  KEY `id_user` (`id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `downloads`
--

DROP TABLE IF EXISTS `downloads`;
CREATE TABLE IF NOT EXISTS `downloads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(500) NOT NULL,
  `tipo` varchar(200) NOT NULL,
  `group` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

DROP TABLE IF EXISTS `eventos`;
CREATE TABLE IF NOT EXISTS `eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_externo` text,
  `nombre_interno` text,
  `emision` year(4) NOT NULL,
  `descripcion` text,
  `id_titular` int(11) NOT NULL,
  `contacto` text,
  `requisitos` text NOT NULL,
  `id_tipo_evento` int(11) DEFAULT NULL,
  `id_idioma` int(11) DEFAULT NULL,
  `id_locacion` int(11) DEFAULT NULL,
  `inscripcion_apertura` datetime DEFAULT NULL,
  `inscripcion_cierre` datetime DEFAULT NULL,
  `cupo_max` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `foto` text,
  `confirmado` tinyint(1) DEFAULT '0',
  `moderador` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tipo_evento` (`id_tipo_evento`),
  KEY `id_idioma` (`id_idioma`),
  KEY `id_lugar` (`id_locacion`),
  KEY `id_tipo_evento_2` (`id_tipo_evento`),
  KEY `id_titular` (`id_titular`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COMMENT='Taba para el registro de talleres, ponencias y papers.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fechas_por_evento`
--

DROP TABLE IF EXISTS `fechas_por_evento`;
CREATE TABLE IF NOT EXISTS `fechas_por_evento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_evento` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_horario` int(11) NOT NULL,
  `hora_inicio` time DEFAULT NULL,
  `hora_fin` time DEFAULT NULL,
  `estado` int(11) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`),
  KEY `id_evento` (`id_evento`),
  KEY `id_evento_2` (`id_evento`),
  KEY `id_horario` (`id_horario`)
) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups_has_services`
--

DROP TABLE IF EXISTS `groups_has_services`;
CREATE TABLE IF NOT EXISTS `groups_has_services` (
  `groups_id` mediumint(8) UNSIGNED NOT NULL,
  `services_id` int(11) NOT NULL,
  PRIMARY KEY (`groups_id`,`services_id`),
  KEY `fk_groups_has_services_services1_idx` (`services_id`),
  KEY `fk_groups_has_services_groups1_idx` (`groups_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios_por_emision`
--

DROP TABLE IF EXISTS `horarios_por_emision`;
CREATE TABLE IF NOT EXISTS `horarios_por_emision` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `emision` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='Contiene los horarios disponibles de cada emisión.';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idiomas`
--

DROP TABLE IF EXISTS `idiomas`;
CREATE TABLE IF NOT EXISTS `idiomas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `locaciones`
--

DROP TABLE IF EXISTS `locaciones`;
CREATE TABLE IF NOT EXISTS `locaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emision` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `cupo_maximo` int(11) NOT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `lugar` varchar(100) DEFAULT NULL,
  `descripcion` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar`
--

DROP TABLE IF EXISTS `lugar`;
CREATE TABLE IF NOT EXISTS `lugar` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_payment` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `bank` varchar(45) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `tax_number` varchar(45) DEFAULT NULL,
  `users_id` int(11) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `organization` varchar(500) NOT NULL,
  `adress` varchar(600) NOT NULL,
  `locality` varchar(100) NOT NULL,
  `postal_code` varchar(45) NOT NULL,
  `country` varchar(100) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `accepted` tinyint(4) DEFAULT '0',
  `discount` double NOT NULL DEFAULT '0',
  `discount_euros` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_order_users1_idx` (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1525 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa`
--

DROP TABLE IF EXISTS `programa`;
CREATE TABLE IF NOT EXISTS `programa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(300) NOT NULL,
  `dia` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `descripcion` varchar(8000) NOT NULL,
  `id_lugar` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recovery`
--

DROP TABLE IF EXISTS `recovery`;
CREATE TABLE IF NOT EXISTS `recovery` (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `email` varchar(500) NOT NULL,
  `institute` varchar(500) NOT NULL,
  `group` varchar(10) NOT NULL,
  `total` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `euro` float DEFAULT NULL,
  `onlyone` tinyint(4) DEFAULT '0',
  `marked` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_logs`
--

DROP TABLE IF EXISTS `tbl_logs`;
CREATE TABLE IF NOT EXISTS `tbl_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `ipaddress` varchar(50) DEFAULT NULL,
  `logtime` datetime DEFAULT NULL,
  `controller` varchar(255) DEFAULT '',
  `action` text,
  `details` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34583 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_de_eventos`
--

DROP TABLE IF EXISTS `tipos_de_eventos`;
CREATE TABLE IF NOT EXISTS `tipos_de_eventos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `afiliation_name` varchar(300) DEFAULT NULL,
  `afiliation_adress` varchar(500) DEFAULT NULL,
  `gaffete` tinyint(4) DEFAULT '0',
  `tittle` varchar(5) DEFAULT NULL,
  `paper_id1` int(11) DEFAULT NULL,
  `paper_id2` int(11) DEFAULT NULL,
  `title1` varchar(500) DEFAULT NULL,
  `title2` varchar(500) DEFAULT NULL,
  `gender` varchar(10) NOT NULL,
  `accept` tinyint(4) NOT NULL,
  `control_num` varchar(12) DEFAULT NULL COMMENT 'Número de control',
  `reg_venue` int(1) NOT NULL DEFAULT '1' COMMENT 'Sede',
  `shirt_size` varchar(3) DEFAULT NULL COMMENT 'Talla camisa',
  `access_code` varchar(25) DEFAULT NULL,
  `registered` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1337 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1782 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_has_services`
--

DROP TABLE IF EXISTS `users_has_services`;
CREATE TABLE IF NOT EXISTS `users_has_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `services_id` int(11) DEFAULT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `total` float DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_has_services_services1_idx` (`services_id`),
  KEY `fk_users_has_services_order1_idx` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1726 DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
