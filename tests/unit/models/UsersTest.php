<?php
namespace tests\models;
use app\models\Users;

class UsersTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = Users::find()->where(['id'=>1]));
        expect($user->username)->equals('alonso');
        expect_not(Users::findIdentity(999));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = Users::findByUsername('alonso'));
        expect_not(User::findByUsername('not-user'));
    }

}