<?php
namespace tests\models;
use app\models\Locaciones;

class LocacionesTest extends \Codeception\Test\Unit
{
    public function testInsertLocacion()
    {
        $locacion = new Locaciones();
        $locacion->nombre = 'Test 1';
        $locacion->cupo_maximo = 50;
        $locacion->direccion = 'Dirección';
        $locacion->lugar = 'Lugaar';
        $locacion->descripcion = 'descripcion';
        $locacion->save();
        expect_that($user = Locaciones::find()->where(['nombre'=>'Test 1']));
        expect($user->username)->equals('Test 1');

        expect_not(Locaciones::find()->where(['nombre'=>'No name']));
    }

/*
    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('100-token'));
        expect($user->username)->equals('admin');

        expect_not(User::findIdentityByAccessToken('non-existing'));        
    }
*/
    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('admin'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('admin');
        expect_that($user->validateAuthKey('test100key'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('admin'));
        expect_not($user->validatePassword('123456'));        
    }

}
