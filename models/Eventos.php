<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "eventos".
 *
 * @property integer $id
 * @property string $nombre_externo
 * @property string $nombre_interno
 * @property string $emision
 * @property string $descripcion
 * @property integer $id_titular
 * @property integer $cupo_max
 * @property string $contacto
 * @property string $requisitos
 * @property integer $id_tipo_evento
 * @property integer $id_idioma
 * @property integer $id_locacion
 * @property string $inscripcion_apertura
 * @property string $inscripcion_cierre
 * @property integer $disponibilidad
 * @property integer $estado
 *
 * @property TipoEvento $tipoEvento
 * @property Idioma $idioma
 * @property Locacion $locacion
 * @property Titular $titular
 * @property Fechas $fechas
 */

class Eventos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'eventos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['foto'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg', 'maxFiles' => 1],
            [['nombre_externo', 'nombre_interno', 'emision', 'id_tipo_evento', 'id_locacion'], 'required'],
            [['emision', 'inscripcion_apertura', 'inscripcion_cierre'], 'safe'],
            [['descripcion', 'requisitos', 'nombre_externo', 'nombre_interno', 'contacto', 'foto', 'moderador'], 'string'],
            [['id_tipo_evento', 'id_idioma', 'id_locacion','id_titular','cupo_max','estado', 'emision'], 'integer'],
            [['fecha'], 'safe'],
	    ['confirmado', 'boolean'],
            [['id_tipo_evento'], 'exist', 'skipOnError' => true, 'targetClass' => Tipos::className(), 'targetAttribute' => ['id_tipo_evento' => 'id']],
            [['id_idioma'], 'exist', 'skipOnError' => true, 'targetClass' => Idiomas::className(), 'targetAttribute' => ['id_idioma' => 'id']],
            [['id_locacion'], 'exist', 'skipOnError' => true, 'targetClass' => Locaciones::className(), 'targetAttribute' => ['id_locacion' => 'id']],
            [['id_titular'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_titular' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'nombre_externo' => \Yii::t('app', 'Título'),
            'nombre_interno' => \Yii::t('app', 'Nombre interno'),
            'emision' => \Yii::t('app', 'Emisión'),
            'descripcion' => \Yii::t('app', 'Descripción'),
            'id_titular' => \Yii::t('app', 'Titular'),
            'fecha' => \Yii::t('app', 'Fecha'),
            'cupo_max' => \Yii::t('app','Cupo máximo'),
            'titular' => \Yii::t('app', 'Titular'),
            'contacto' => \Yii::t('app', 'Contacto'),
            'requisitos' => \Yii::t('app', 'Requisitos'),
            'id_tipo_evento' => \Yii::t('app', 'Tipo de evento'),
            'id_idioma' => \Yii::t('app', 'Idioma'),
            'id_locacion' => \Yii::t('app', 'Locación'),
            'inscripcion_apertura' => \Yii::t('app', 'Apertura de inscrpción'),
            'inscripcion_cierre' => \Yii::t('app', 'Cierre de inscripción'),
            'estado' => \Yii::t('app', 'Permitir registro de asistentes?'),
            'foto' => \Yii::t('app', 'Foto / Imagen'),
	    'confirmado' => \Yii::t('app','Confirmado'),
	    'moderador' => \Yii::t('app','Moderador'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoEvento()
    {
        return $this->hasOne(Tipos::className(), ['id' => 'id_tipo_evento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTitular()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_titular']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFechas()
    {
        return $this->hasMany(Fechas::className(), ['id_evento' => 'id'])->orderBy(['fecha'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdioma()
    {
        return $this->hasOne(Idiomas::className(), ['id' => 'id_idioma']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocacion()
    {
        return $this->hasOne(Locaciones::className(), ['id' => 'id_locacion']);
    }
    
    /**
    * @ return \yii\db\ActiveQuery
    */
    public function getDisponibilidad()
    {
        $evento = Eventos::findOne(['id' => $this->id]);
        $max = $evento->cupo_max;
        $asistentes = Asistencia::find()->where(['id_evento'=>$this->id])->all();
        $actual = count($asistentes);
        return $max - $actual;
    }

    /**
    * @ return integer
    */
    public function verificarRegistro($id_user)
    {
        try {
            $user = Users::findOne(['id' => $id_user]);
            $asistencia = Asistencia::find()->where(['id_evento'=>$this->id, 'id_user'=>$id_user])->count();
            if ($asistencia >= 1) return 1; // Usuario registrado
            if($this->estado == false) return 0; // no se permite registro
            if($this->confirmado == false) return 0; // evento sin confirmar
            switch($this->tipoEvento->id)
            {
                case 7:
                case 8:
                case 5:
                    return 0; // No se permite registrar a eventos: sociales, privados o de registro.
                    break;
            }
            $asistentes = Asistencia::find()->where(['id_evento'=>$this->id])->count();
            if($this->cupo_max <= $asistentes) return 0; //se encuentra lleno este evento

            //que el usuario no tenga horarios solapados
            $horarios_de_evento = Fechas::find()
                ->where(['id_evento'=>$this->id])
                ->all();
            //obtenemos los eventos registrados para el usuario
            $idEventosQuery = (new Query())
                ->select('id_evento')
                ->from('asistentes_por_fecha')
                ->where(['id_user' => $id_user]);
            $eventos_de_usuario = Eventos::find()
                ->select('id')
                ->where(['id'=>$idEventosQuery]);
            
            // obtenemos los horarios registrados para el usuario
            $horarios_de_usuario = Fechas::find()
                ->where(['id_evento' => $eventos_de_usuario])
                ->all();
            
            // Se revisa que no tenga horarios iguales el mismo dia
            foreach($horarios_de_usuario as $horario_de_usuario)
            {
                foreach($horarios_de_evento as $horario_de_evento)
                {
                    if(($horario_de_usuario->fecha == $horario_de_evento->fecha) && ($horario_de_usuario->id_horario == $horario_de_evento->id_horario))
                    {
                        return 0; //Conflicto de horarios con el evento
                    }
                }
            }
            return -1; // no se encontró registro ni conflicto
        } catch (Exception $e) {
            return false;
        }
    }
}
