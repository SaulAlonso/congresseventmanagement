<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'forgotten_password_time', 'created_on', 'last_login', 'active', 'gaffete', 'paper_id1', 'paper_id2', 'accept', 'reg_venue', 'registered', 'certificado_enviado'], 'integer'],
            [['ip_address', 'username', 'password', 'salt', 'email', 'activation_code', 'forgotten_password_code', 'remember_code', 'name', 'city', 'country', 'afiliation_name', 'afiliation_adress', 'tittle', 'title1', 'title2', 'gender', 'control_num', 'shirt_size', 'access_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'forgotten_password_time' => $this->forgotten_password_time,
            'created_on' => $this->created_on,
            'last_login' => $this->last_login,
            'active' => $this->active,
            'gaffete' => $this->gaffete,
            'paper_id1' => $this->paper_id1,
            'paper_id2' => $this->paper_id2,
            'accept' => $this->accept,
            'reg_venue' => $this->reg_venue,
            'registered' => $this->registered,
            'certificado_enviado' => $this->certificado_enviado,
        ]);

        $query->andFilterWhere(['like', 'ip_address', $this->ip_address])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'salt', $this->salt])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'activation_code', $this->activation_code])
            ->andFilterWhere(['like', 'forgotten_password_code', $this->forgotten_password_code])
            ->andFilterWhere(['like', 'remember_code', $this->remember_code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'afiliation_name', $this->afiliation_name])
            ->andFilterWhere(['like', 'afiliation_adress', $this->afiliation_adress])
            ->andFilterWhere(['like', 'tittle', $this->tittle])
            ->andFilterWhere(['like', 'title1', $this->title1])
            ->andFilterWhere(['like', 'title2', $this->title2])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'control_num', $this->control_num])
            ->andFilterWhere(['like', 'shirt_size', $this->shirt_size])
            ->andFilterWhere(['like', 'access_code', $this->access_code]);

        return $dataProvider;
    }
}
