<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "eventos".
 *
 * @property integer $id
 * @property string $correo
 *
 */

class CorreoMasivo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'correos_para_envio_masivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['correo'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'correo' => \Yii::t('app', 'Correo'),
        ];
    }
}
