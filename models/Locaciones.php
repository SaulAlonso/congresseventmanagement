<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locaciones".
 *
 * @property integer $id
 * @property string $nombre
 * @property integer $emision
 */
class Locaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre','emision'], 'required'],
            [['emision'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'nombre' => \Yii::t('app', 'Nombre'),
            'emision' => \Yii::t('app', 'Emision'),
        ];
    }
}
