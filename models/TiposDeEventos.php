<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipos_de_eventos".
 *
 * @property integer $id
 * @property string $nombre
 */
class TiposDeEventos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipos_de_eventos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'nombre' => \Yii::t('app', 'Nombre'),
        ];
    }
}
