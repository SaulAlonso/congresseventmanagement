<?php
/*
* En este modelo se instancian los eventos a bloques de tiempo y locaciones para que aparezcan en el programa general
*/
namespace app\models;

use Yii;

/**
 * This is the model class for table "horarios_por_evento".
 *
 * @property integer $id
 * @property integer $id_evento
 * @property string $fecha
 * @property integer $id_horario
 * @property string $estado
 * @property boolean $eventos_en_paralelo
 * @property integer $_rowspan
 */
class Fechas extends \yii\db\ActiveRecord
{
    private $_rowspan =null; // se agrego para almacenar los bloques de tiempo que cubre el evento.
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'horarios_por_evento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_evento', 'fecha', 'id_horario'], 'required'],
            [['id_evento', 'estado', 'id_horario'], 'integer'],
            [['fecha'], 'safe'],
            [['eventos_en_paralelo'], 'boolean'],
            [['id_evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['id_evento' => 'id']],
            [['id_horario'], 'exist', 'skipOnError' => true, 'targetClass' => HorariosPorEmision::className(), 'targetAttribute' => ['id_horario' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'id_evento' => \Yii::t('app', 'Nombre externo del evento'),
            'fecha' => \Yii::t('app', 'Fecha'),
            'id_horario' => \Yii::t('app', 'Bloque de tiempo'),
            'estado' => \Yii::t('app', 'Mostrar en asistencia'),
            'eventos_en_paralelo' => \Yii::t('app', 'Tiene eventos en paralelo?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasOne(Eventos::className(), ['id' => 'id_evento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHorario()
    {
        return $this->hasOne(HorariosPorEmision::className(), ['id' => 'id_horario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventByHourLocacion($id_horario, $id_locacion, $fecha)
    {
        $row = Fechas::find()
            ->leftJoin('eventos', 'horarios_por_evento.id_evento = eventos.id')
            ->leftJoin('locaciones', 'eventos.id_locacion = locaciones.id')
            ->leftJoin('horarios_por_emision', 'horarios_por_evento.id_horario = horarios_por_emision.id')
            ->where(['locaciones.id'=>$id_locacion, 'horarios_por_emision.id'=>$id_horario, 'horarios_por_evento.fecha'=>$fecha])
            ->one();
        return $row;
    }

    /**
     * @return Boolean
     */
    public function beforeSave($new){
        // revisa que no este registrado
        $registros_similares = count($this::find()->where(['id_horario'=>$this->id_horario, 'fecha'=>$this->fecha, 'id_evento'=>$this->id_evento])->all());
        if($this->isNewRecord && $registros_similares >= 1)
        {
            $this->addError('id_horario',\Yii::t('app','Ya se encuentra registrado en este horario.'));
            return false;
        }
        
        // revisa que no tenga conflictos si fue asignado un evento sin eventos en paralelo
        $eventos_con_mismo_horario_sin_eventos_en_paralelo = $this::find()->where(['id_horario'=>$this->id_horario, 'fecha'=>$this->fecha, 'eventos_en_paralelo'=>false])->count();
        if($eventos_con_mismo_horario_sin_eventos_en_paralelo >= 1)
        {
            $this->addError('eventos_en_paralelo',\Yii::t('app','Ya se encuentra registrado un evento en este horario marcado sin eventos en paralelo.'));
            return false;
        }
        
        // si el evento fue marcado sin eventos en paralelo revisa que no existan eventos asignados
        if($this->eventos_en_paralelo == false)
        {
    	    $eventos_con_el_mismo_horario = $this::find()->where(['id_horario'=>$this->id_horario, 'fecha'=>$this->fecha])->count();
    	    if($this->isNewRecord && $eventos_con_el_mismo_horario >= 1)
    	    {
        	$this->addError('eventos_en_paralelo',\Yii::t('app','Ya se encuentran registrados eventos en este horario.'));
        	return false;
    	    }
        }
        
        return true;
    }

    public function getRowspan(){
         return $this->_rowspan;
     }

     public function setRowspan($rowspan){
         $this->_rowspan = $rowspan;
     }
}
