<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_logs".
 *
 * @property integer $id
 * @property string $username
 * @property string $ipaddress
 * @property string $logtime
 * @property string $controller
 * @property string $action
 * @property string $details
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'details'], 'string'],
            [['username', 'ipaddress'], 'string', 'max' => 50],
            [['controller'], 'string', 'max' => 255],
            [['logtime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'ipaddress' => 'Ipaddress',
            'logtime' => 'Logtime',
            'controller' => 'Controller',
            'action' => 'Action',
            'details' => 'Details',
            'logtime' => 'Log Time',
        ];
    }
}
