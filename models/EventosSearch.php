<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Eventos;

/**
 * EventosSearch represents the model behind the search form about `app\models\Eventos`.
 */
class EventosSearch extends Eventos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['descripcion', 'requisitos', 'nombre_externo', 'nombre_interno', 'contacto', 'moderador'], 'string'],
            [['id_tipo_evento', 'id_idioma', 'id_locacion','id_titular','cupo_max','estado', 'emision'], 'integer'],
            [['fecha'], 'safe'],
            ['confirmado', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Eventos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'emision' => ($this->emision == "")?\Yii::$app->params["emision"]:$this->emision,
            'id_tipo_evento' => $this->id_tipo_evento,
            'id_idioma' => $this->id_idioma,
            'id_locacion' => $this->id_locacion,
            'fecha' => $this->fecha,
            'cupo_max' => $this->cupo_max,
            'inscripcion_apertura' => $this->inscripcion_apertura,
            'inscripcion_cierre' => $this->inscripcion_cierre,
        ]);

        $query->andFilterWhere(['like', 'nombre_externo', $this->nombre_externo])
            ->andFilterWhere(['like', 'nombre_interno', $this->nombre_interno])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'moderador', $this->moderador])
            ->andFilterWhere(['like', 'contacto', $this->contacto]);

        return $dataProvider;
    }
}
