<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Fechas;

/**
 * FechasSearch represents the model behind the search form about `app\models\Fechas`.
 */
class FechasSearch extends Fechas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_evento', 'estado', 'id_horario'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Fechas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->fecha == date('1970-01-01') || $this->fecha == '')
            $this->fecha = null;

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_evento' => $this->id_evento,
            'fecha' => $this->fecha,
            'estado' => $this->estado,
            'id_horario' => $this->id_horario,
        ]);

	$query->andFilterWhere(['like', 'fecha', (($this->fecha == "")? \Yii::$app->params['emision']:$this->fecha)]);

        return $dataProvider;
    }
}
