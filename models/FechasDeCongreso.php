<?php
/*
*** Modelo para la gesti�n de las fechas para los 3 días del congreso
*/
namespace app\models;

use Yii;

/**
 * This is the model class for table "fechas_por_evento".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $emision
 * @property boolean $horario_de_registro
 *
 */
class FechasDeCongreso extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fechas_de_congreso';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha', 'emision'], 'required'],
            [['emision'], 'integer'],
            [['fecha'], 'safe'],
            [['horario_de_registro'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'Id'),
            'fecha' => \Yii::t('app', 'Fecha'),
            'horario_de_registro' => \Yii::t('app', 'Tiene horario de registro?'), 
        ];
    }


    /**
     * @return Boolean
     */
    public function beforeSave($new){
        $rows = count($this::find()->where(['emision'=>$this->emision, 'fecha' =>$this->fecha])->all());
        if($this->isNewRecord && $rows >= 1)
            return false;
        return true;
    }
}
