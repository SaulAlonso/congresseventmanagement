<?php

namespace app\models;

use Yii;
use app\models\Groups;
/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property resource $ip_address
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $activation_code
 * @property string $forgotten_password_code
 * @property integer $forgotten_password_time
 * @property string $remember_code
 * @property integer $created_on
 * @property integer $last_login
 * @property integer $active
 * @property string $name
 * @property string $city
 * @property string $country
 * @property string $afiliation_name
 * @property string $afiliation_adress
 * @property integer $gaffete
 * @property string $tittle
 * @property integer $paper_id1
 * @property integer $paper_id2
 * @property string $title1
 * @property string $title2
 * @property string $gender
 * @property integer $accept
 * @property string $control_num
 * @property integer $reg_venue
 * @property string $shirt_size
 * @property string $access_code
 * @property Group $group
 * @property integer $certificado_enviado
 */
class Users extends \yii\db\ActiveRecord  implements \yii\web\IdentityInterface
{
    const ADMIN = 1;
    const PONENTE = 2;
    const PUBLICO = 3;
        /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ip_address', 'username', 'password', 'email', 'created_on', 'gender', 'accept'], 'required'],
            [['forgotten_password_time', 'created_on', 'last_login', 'active', 'gaffete', 'paper_id1', 'paper_id2', 'accept', 'reg_venue','registered', 'certificado_enviado'], 'integer'],
            [['ip_address'], 'string', 'max' => 16],
            [['username', 'email', 'name', 'country'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 80],
            [['salt', 'activation_code', 'forgotten_password_code', 'remember_code'], 'string', 'max' => 40],
            [['city'], 'string', 'max' => 50],
            [['afiliation_name'], 'string', 'max' => 300],
            [['afiliation_adress', 'title1', 'title2'], 'string', 'max' => 500],
            [['tittle'], 'string', 'max' => 5],
            [['gender'], 'string', 'max' => 10],
            [['control_num'], 'string', 'max' => 12],
            [['shirt_size'], 'string', 'max' => 3],
            [['access_code'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' =>  \Yii::t('app', 'Id'),
            'ip_address' =>  \Yii::t('app', 'Dirección IP'),
            'username' =>  \Yii::t('app', 'Nombre de usuario'),
            'password' =>  \Yii::t('app', 'Contraseña'),
            'salt' => \Yii::t('app', 'Salt'),
            'email' => \Yii::t('app', 'Correo'),
            'activation_code' => \Yii::t('app', 'Codigo de activación'),
            'forgotten_password_code' => 'Forgotten Password Code',
            'forgotten_password_time' => 'Forgotten Password Time',
            'remember_code' => 'Remember Code',
            'created_on' => 'Created On',
            'last_login' => 'Last Login',
            'active' => 'Active',
            'name' => \Yii::t('app', 'Nombre'),
            'city' => \Yii::t('app', 'Ciudad'),
            'country' => \Yii::t('app', 'Nación'),
            'afiliation_name' => \Yii::t('app', 'Nombre de aficiación'),
            'afiliation_adress' => \Yii::t('app', 'Dirección de afiliación'),
            'gaffete' => 'Gaffete',
            'tittle' => 'Tittle',
            'paper_id1' => 'Paper Id1',
            'paper_id2' => 'Paper Id2',
            'title1' => 'Title1',
            'title2' => 'Title2',
            'gender' => 'Gender',
            'accept' => 'Accept',
            'control_num' => 'Control Num',
            'reg_venue' => 'Reg Venue',
            'shirt_size' => 'Shirt Size',
            'access_code' => 'Access Code',
	    'registered'=>'Registered',
	    'certificado_enviado'=>'Certificado enviado'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrupo()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id'])->viaTable('users_groups', ['user_id' => 'id']);
    }

    public function getRegistered()
    {
	return $this->registered;
    }

    /**
     * @inheritdoc
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @inheritdoc
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @inheritdoc
     */
    public function validateId($id)
    {
        return $this->id === $id;
    }

    /**
     * @inheritdoc
     */
    public function validateUsername($username)
    {
        return $this->username === $username;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

   /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
}
