<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FechasDeCongreso;

/**
 * FechasDeCongresoSearch represents the model behind the search form about `app\models\FechasDeCongreso`.
 */
class FechasDeCongresoSearch extends FechasDeCongreso
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'emision'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FechasDeCongreso::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha' => $this->fecha,
            'emision' => ($this->emision == "")?\Yii::$app->params["emision"]:$this->emision,
        ]);

        return $dataProvider;
    }
}
