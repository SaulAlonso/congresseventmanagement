<?php
/*
** Este es el controlador para registrar asistentes a eventos, para reglas de negocio o demas validaciones al registrar a eventos realizarlo aqui
*/
namespace app\models;

use Yii;
use app\models\Eventos;
use yii\db\Query;

/**
 * This is the model class for table "asistentes_por_fecha".
 *
 * @property integer $id_user
 * @property integer $id_evento
 * 
 * @property Evento $evento
 */
class Asistencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'asistentes_por_fecha';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'id_evento'], 'required'],
            [['id_user', 'id_evento'], 'integer'],
            [['id_evento'], 'exist', 'skipOnError' => true, 'targetClass' => Eventos::className(), 'targetAttribute' => ['id_evento' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => \Yii::t('app', 'Usuario'),
            'id_evento' => \Yii::t('app', 'Evento'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvento()
    {
        return $this->hasOne(Eventos::className(), ['id' => 'id_evento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

    public function beforeSave($new){
        // revisa que el evento tenga habilitado el registro de asistentes
        if($this->evento->estado == false)
        {
            $this->addError('id_evento', \Yii::t('app','El evento no permite el registro de asistentes, revise configuracion.'));
            return false;
        }

        // revisa que el evento este confirmado
        if($this->evento->confirmado == false)
        {
            $this->addError('id_evento', \Yii::t('app','El evento aun no ha sido confirmado.'));
            return false;
        }

        // que no se registre a un evento social (7), registro (8) o privado (5)
    	switch($this->evento->tipoEvento->id)
        {
    	    case 7:
    	    case 8:
    	    case 5:
    	        $this->addError('id_evento', \Yii::t('app', 'No se permite registrar a eventos: sociales, privados o de registro.'));
    		return false;
    		break;
        }
        
        //que no se registre al mismo evento
        $registrado = count($this::find()
                            ->where(['id_evento'=>$this->id_evento])
                            ->andWhere(['and',['id_user'=>$this->id_user]])
                            ->all());
        if($registrado)
        {
            $this->addError('id_evento', \Yii::t('app','Ya se encuentra registrado a este evento.'));
            return false;
        }

        //que no supere el maximo del lugar
        $asistentes = count($this::find()->where(['id_evento'=>$this->id_evento])->all());
        $sql = "SELECT cupo_max FROM eventos
        where id = $this->id_evento";
        $db = \Yii::$app->db;
        $data = $db->createCommand($sql)->queryOne();       
        if($data['cupo_max'] <= $asistentes)
        {
            $this->addError('id_evento', \Yii::t('app','Ya se encuentra lleno este evento.'));
            return false;
        }

        //que el usuario no tenga horarios solapados
        // obtenemos todos los horarios asignados al evento a agregar
        $horarios_de_evento = Fechas::find()
    	    ->where(['id_evento'=>$this->id_evento])
    	    ->all();

        //obtenemos los eventos registrados para el usuario
        $idEventosQuery = (new Query())
    	    ->select('id_evento')
    	    ->from('asistentes_por_fecha')
    	    ->where(['id_user' => $this->id_user]);
        $eventos_de_usuario = Eventos::find()
    	    ->select('id')
    	    ->where(['id'=>$idEventosQuery]);
    	
	// obtenemos los horarios registrados para el usuario
	$horarios_de_usuario = Fechas::find()
	    ->where(['id_evento' => $eventos_de_usuario])
	    ->all();
	
	// Se revisa que no tenga horarios iguales el mismo dia
	foreach($horarios_de_usuario as $horario_de_usuario)
	{
	    foreach($horarios_de_evento as $horario_de_evento)
	    {
		if(($horario_de_usuario->fecha == $horario_de_evento->fecha) && ($horario_de_usuario->id_horario == $horario_de_evento->id_horario))
		{
            	    $this->addError('id_evento', \Yii::t('app','Conflicto de horarios con el evento: ').$horario_de_usuario->evento->nombre_externo);
		    return false;
		}
	    }
	}
        return true;
    }
}
