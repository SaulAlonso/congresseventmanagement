<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "horarios_por_emision".
 *
 * @property integer $id
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property integer $emision
 * @property boolean $hora_de_registro True si este bloque de tiempo es para registro
 */
class HorariosPorEmision extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'horarios_por_emision';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hora_inicio', 'hora_fin', 'emision'], 'required'],
            [['hora_inicio', 'hora_fin'], 'safe'],
            [['emision'], 'integer'],
            [['hora_de_registro'],'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'hora_inicio' => \Yii::t('app','Hora de inicio'),
            'hora_fin' => \Yii::t('app','Hora de fin'),
            'emision' => \Yii::t('app','Emision'),
            'hora_de_registro' => \Yii::t('app','Este bloque de tiempo es para registro de asistentes?'),
        ];
    }
}
