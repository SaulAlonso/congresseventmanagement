<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HorariosPorEmision;

/**
 * HorariosPorEmisionSearch represents the model behind the search form about `app\models\HorariosPorEmision`.
 */
class HorariosPorEmisionSearch extends HorariosPorEmision
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'emision'], 'integer'],
            [['hora_inicio', 'hora_fin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HorariosPorEmision::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'hora_inicio' => $this->hora_inicio,
            'hora_fin' => $this->hora_fin,
            'emision' => ($this->emision == "" )? \Yii::$app->params['emision']:$this->emision,
        ]);

        return $dataProvider;
    }
}
