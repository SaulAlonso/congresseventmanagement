<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Idiomas */

?>
<div class="fechas-view">
<?=($model->foto!='')?Html::img($model->foto, ['style' => 'width: 50%; margin: 0 auto;','class'=>'img-responsive']):null;
?>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'nombre_externo',
                'value' => strip_tags($model->nombre_externo),
            ],
            /*[
                'attribute' => 'contacto',
                'value' => $model->contacto,
            ],*/ 
            [
                'attribute' => 'descripcion',
                'value' => ($model->descripcion!='')?$model->descripcion:\Yii::t('app','Sin descripción...'),
            ],
            [
                'attribute' => 'requisitos',
                'value' => ($model->requisitos!='')?$model->requisitos:\Yii::t('app','Sin requisitos...'),
            ],
            [
                'attribute' => 'disponibilidad',
                'value' => ($model->disponibilidad<=0)?\Yii::t('app', 'Lleno'):$model->disponibilidad,
            ],
            /*[
                'attribute' => 'confirmado',
                'label' => '',
                'value' => ($model->confirmado==false)?\Yii::t('app','Por confirmar'):'',
            ],*/
        ],
    ]) ?>

</div>
