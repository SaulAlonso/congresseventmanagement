<?php

namespace yii\jui;

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = \Yii::t('app', 'Programa General');
/* @var $this yii\web\View */
/* @var $fechas yii\data\ActiveDataProvider */
/* @var $asistencia yii\data\ActiveDataProvider */
/* @var $result yii\data\ActiveDataProvider */
$this->registerCssFile("@web/css/style.css");
//$this->registerJsFile("@web/js/script.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="div-sections">
  <img class="img cimps-logo" src="<?=\Yii::$app->params['logoEmision']?>"/>
  <img class="img cimat-logo" src="img/cimat-logo.png"/>
  <img class="img pnpc-logo" src="img/pnpc.png"/>
</div>
<br>
<div class="div-sections" >
  <h1><?=\Yii::t('app', 'Programa General')?></h1>
  <br>
  <br>
    <?=(isset($fechas[0]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[0]->fecha, 'dd'):'' ?>
    <?= $this->render('_tab_pdf', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia1,
                    'eventos' => $eventosdia1,
                    ]
                )
                ?>
  <br>
    <?=(isset($fechas[1]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[1]->fecha, 'dd'):'' ?>
    <?= $this->render('_tab_pdf', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia2,
                    'eventos' => $eventosdia2,
                    ]
                )
                ?>
  <br>
    <?=(isset($fechas[2]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[2]->fecha, 'dd'):'' ?>
    <?= $this->render('_tab_pdf', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia3,
                    'eventos' => $eventosdia3,
                    ]
                )
                ?>
  <br>
