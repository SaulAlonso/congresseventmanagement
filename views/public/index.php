<?php

namespace yii\jui;

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = \Yii::t('app', 'Programa General');
/* @var $this yii\web\View */
/* @var $fechas yii\data\ActiveDataProvider */
/* @var $asistencia yii\data\ActiveDataProvider */
/* @var $result yii\data\ActiveDataProvider */

//$this->registerJsFile("@web/js/script.js", ['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="div-sections">
  <img class="img cimps-logo" src="<?=\Yii::$app->params['logoEmision']?>"/>
  <img class="img cimat-logo" src="img/cimat-logo.png"/>
  <img class="img pnpc-logo" src="img/pnpc.png"/>
</div>
<br>
<div class="div-sections">
  <h1><?=\Yii::t('app', 'Programa General')?></h1>
    <a target="_blank" href='resources/Programa_CIMPS_2018.pdf'><img title='Descargar programa' style='width: 5%;' src='img/PDF-Icon-Download.png'/></a>
  <?php
    if(isset(\Yii::$app->user->identity->name) && \Yii::$app->user->identity->name == "Admin")
    {?>
    <a target="_blank" href='resources/Programa_CIMPS_2018.pdf'><img title='Descargar programa' style='width: 5%;' src='img/PDF-Icon-Download.png'/></a>
    <?php }
  ?>
  <br>
  <br>
  <?=Tabs::widget([
      'items' => [
          [
              'label' => (isset($fechas[0]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[0]->fecha, 'dd'):'',
              'options' => ['id' => 'tab1'],
              'content' => $this->render('_tab', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia1,
                    'eventos' => $eventosdia1,
                    ]
                ),
          ],
          [
              'label' => (isset($fechas[1]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[1]->fecha, 'dd'):'',
              'options' => ['id' => 'tab2'],
              'content' => $this->render('_tab', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia2,
                    'eventos' => $eventosdia2,
                    ]
                ),
          ],
          [
              'label' => (isset($fechas[2]->fecha))?'Oct '.\Yii::$app->formatter->asDate($fechas[2]->fecha, 'dd'):'',
              'options' => ['id' => 'tab3'],
              'content' => $this->render('_tab', 
              	    [
                    'locaciones'=> $locaciones,
                    'horarios' => $horariodia3,
                    'eventos' => $eventosdia3,
                    ]
                ),
          ],
      ],
  ]);
 ?>
 </div>
