<?php
use app\models\Fechas;
/* @var $this yii\web\View */
/* @var $result yii\data\ActiveDataProvider */
?>
<div style="overflow-x:auto">
<br>
<table class="table h6 " >
    <tr class="tab-row-title">
        <td>
        <div style="margin-top: 8px;">
            <?=\Yii::t('app', 'Horario')?>
            </div>
        </td>
        <?php // headers de locaciones 
        foreach ($locaciones as $locacion) 
        {
        ?>
        <td>
            <div class='locationtype<?= $locacion->id ?>' style='margin-top: 1px;'>
            <?=\Yii::t('app', $locacion->nombre)?>
            </div>
        </td>
        <?php 
        }
        ?>
    </tr>
    <tr>
    	<td colspan="8">
	   </td>
    </tr>
    <?php // para imprimir el programa general
    $i = 0;
    foreach ($horarios as $horario) 
    {
        $j = 0;
    ?>
        <tr class='tr-table'>
            <td class='td-horario'>
                <div style='margin-top: 8px;'>
                    <?=date("h:i", strtotime($horario->hora_inicio))
                    ." - "
                    .date("h:i A", strtotime($horario->hora_fin))
                    ?>
                </div>
            </td>
            <?php
            foreach ($locaciones as $locacion) 
            {
                $data = $eventos[$i][$j];
                if($data)// sí hay evento instanciado se imprime
                {
            	    if($data->evento->tipoEvento->id == 10) // id de evento vacio
            	    {
                	echo "<td class='td-all-row locationtype".$data->evento->locacion->id."' colspan=1 rowspan=".$data->rowspan.">
                    	    <div style='margin-top: 8px;'>
                    	    </div>
                	</td>";    
            	    }else if($data->eventos_en_paralelo)
            	    {
            		echo "<td class='td-all-row locationtype".$data->evento->locacion->id."' colspan=1 rowspan=".$data->rowspan.">
                    	    <div style='margin-top: 8px;'>";
                	echo $this->render('_thumb_pdf', ['data' => $data]);
                	echo "</div>
                	</td>";
            	    }else
            	    {
            		switch($data->evento->tipoEvento->id)
            		{
            		    case 7: //evento social
                	    case 8: // registro | inauguracion
                	    case 9: // Refrigerio
            			echo "<td class='td-all-row eventtype".$data->evento->tipoEvento->id."' colspan=".count($locaciones)." rowspan=".$data->rowspan.">
                    	    	    <div style='margin-top: 8px;'>";
                		echo "<p style='font-weight: bold;'>".$data->evento->nombre_externo."</p>";
                		echo "</div></td>";
                		break;
                	    default:
                		// imprime evento academico sin eventos en paralelo.
                		echo "<td class='td-all-row locationtype".$data->evento->locacion->id."' colspan=".count($locaciones)." rowspan=".$data->rowspan.">
                    		    <div style='margin-top: 8px;'>";
                    		echo $this->render('_thumb_pdf', ['data' => $data]);
                		echo "</div></td>";
                	}
            	    }
                }else // si no hay evento se imprime bloque vacio.
                {
                    /*echo "<td class='td-all-row '>
                        <div style='margin-top: 8px;'>
                        </div>
                    </td>"; */   
                }
                $j++;
            }
            ?>
        </tr>    
    <?php 
    $i++;
    }
    ?>
</table>
</div>
