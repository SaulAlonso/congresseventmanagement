<?php

namespace yii\jui;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Users;
/* @var $data array */

$tipos_de_eventos = array(
    0 => \Yii::t('app', 'Evento social'),
    1 => \Yii::t('app', 'Conferencia'),
    2 => \Yii::t('app','Taller'),
    3 => \Yii::t('app','Artículo'),
    4 => \Yii::t('app', 'Networking'),
    5 => \Yii::t('app', 'Evento privado'),
    6 => \Yii::t('app', 'Registro'),
 );

if($data){
	$disponibilidad = ($data->evento->disponibilidad==0)?\Yii::t('app', 'Lleno'):$data->evento->disponibilidad;
	$return ='<p><label><span>'.$data->evento->nombre_externo.'</span></label>';
	$return .= ($data->evento->contacto=='')?'':'<br>'.$data->evento->contacto; //$data->titular->name;
	$return .= '<br>'.\Yii::t('app', 'Tipo').': '.$data->evento->tipoEvento->nombre;
	$return .= '<br><label>'.\Yii::t('app', 'Moderador').': '.$data->evento->moderador.'</label>';
	$return .= '<br>'.\Yii::t('app', 'Disponibilidad').': '.$disponibilidad;
	//$return .= ($data->evento->confirmado==false)?'<div style="color:red;">'.\Yii::t('app','Por confirmar').'</div>':'';
	$return .= '<br><div class="style-link">'.Html::a(
            \Yii::t('app', 'Ver detalles'),
            FALSE,
            [
                'title' => \Yii::t('app', 'Detalles'),
                'value' => Url::to(['public/detalle', 'id' => $data->evento->id]),
                'class' => 'show-modal-button',
                'style' => 'cursor: pointer;',
            ]
        ).'</div>';
    if(isset(\Yii::$app->user->identity->grupo) && \Yii::$app->user->identity->grupo->id==Users::ADMIN)
    {
	$return .= Html::a(\Yii::t('app', 'Editar'), ['/eventos/update', 'id' => $data->evento->id], ['target'=>'_blank','class' => '']).'<br>';
    }
    if (isset(\Yii::$app->user->identity) && \Yii::$app->user->identity->id > 0 )
        {
        $return .='<div>';
            // se imprime el icono deacuerdo al estatus de cada evento
            switch ($data->evento->verificarRegistro(\Yii::$app->user->identity->id)) 
            {
                case -1:
        	    $return .='<div  style="cursor:pointer;" class="register" data-event="'.$data->evento->id.'">';
                    $return .='<i class="fa fa-plus-square-o  fa-3x" aria-hidden="true"></i>'; // permite registro
                    break;
                case 0:
        	    $return .='<div  style="cursor:default;" data-event="'.$data->evento->id.'">';
                    $return .='<i class="fa fa-ban  fa-3x" aria-hidden="true"></i>'; // registro no permitido
                    break;
                case 1:
        	    $return .='<div  style="cursor:default;" data-event="'.$data->evento->id.'">';
                    $return .='<i class="fa fa-check  fa-3x" aria-hidden="true"></i>'; // registrado
                    break;
            }
            $return .='</div>';
        $return .='</div>';
        }
}else{
 	$return ='<p><label><span></span></label>';
}
print_r($return);
?>
