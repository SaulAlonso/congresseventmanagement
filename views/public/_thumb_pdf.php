<?php

namespace yii\jui;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Users;
/* @var $data array */

$tipos_de_eventos = array(
    0 => \Yii::t('app', 'Evento social'),
    1 => \Yii::t('app', 'Conferencia'),
    2 => \Yii::t('app','Taller'),
    3 => \Yii::t('app','Artículo'),
    4 => \Yii::t('app', 'Networking'),
    5 => \Yii::t('app', 'Evento privado'),
    6 => \Yii::t('app', 'Registro'),
 );

if($data){
	$disponibilidad = ($data->evento->disponibilidad==0)?\Yii::t('app', 'Lleno'):$data->evento->disponibilidad;
	$return ='<p><label><span>'.$data->evento->nombre_externo.'</span></label>';
	$return .= ($data->evento->contacto=='')?'':'<br>'.$data->evento->contacto; //$data->titular->name;
	$return .= '<br>'.\Yii::t('app', 'Tipo').': '.$data->evento->tipoEvento->nombre;
	$return .= '<br><label>'.\Yii::t('app', 'Moderador').': '.$data->evento->moderador.'</label>';
        $return .='</div>';
}else{
 	$return ='<p><label><span></span></label>';
}
print_r($return);
?>
