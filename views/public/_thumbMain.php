<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $data array */

$tipos_de_eventos = array(
	0 => \Yii::t('app', 'Evento social'),
    1 => \Yii::t('app', 'Conferencia'),
    2 => \Yii::t('app','Taller'),
    3 => \Yii::t('app','Artículo'),
    4 => \Yii::t('app', 'Networking'),
    5 => \Yii::t('app', 'Evento privado'),
 );
if($data){
	$return ='<p><label><span>'.$data->evento->nombre_externo.'</span>';
	$return .= ' '.\Yii::t('app', 'Moderador').': '.$data->evento->moderador.'</label>';
}else{
 	$return ='<p><label><span></span></label>';
}
print_r($return);
?>
