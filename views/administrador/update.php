<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Asistencia */

$this->title = \Yii::t('app', 'Actualizar asistencia'). ": " . $model->id_user;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_user, 'url' => ['view', 'id_user' => $model->id_user, 'id_evento' => $model->id_evento, 'usuarios'=>$usuarios,'eventos'=>$eventos]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Actualizar');
?>
<div class="asistencia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'usuarios' => $usuarios,
        'eventos' => $eventos,
    ]) ?>

</div>
