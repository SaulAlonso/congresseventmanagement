<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Asistencia */

$this->title = \Yii::t('app', 'Crear asistencia');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'usuarios' =>$usuarios,
        'eventos' => $eventos,
    ]) ?>

</div>
