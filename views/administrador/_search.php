<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\AsistenciaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asistencia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_user')->dropDownList(ArrayHelper::map($usuarios, 'id', 'name'), ['prompt' => \Yii::t('app', 'Seleccione Uno') ]) ?>
    
    <?= $form->field($model, 'id_evento')->dropDownList(ArrayHelper::map($eventos, 'id', 'nombre_externo'), ['prompt' => \Yii::t('app','Seleccione Uno') ]) ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Buscar'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(\Yii::t('app', 'Refrescar'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
