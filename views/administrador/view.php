<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Asistencia */

$this->title = $model->id_user;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Asistencias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Registrar asistencia'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Actualizar'), ['update', 'id_user' => $model->id_user, 'id_evento' => $model->id_evento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Eliminar'), ['delete', 'id_user' => $model->id_user, 'id_evento' => $model->id_evento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Confirma eliminar este registro?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_user',
                'value' => $model->user->username
            ],
            [
                'attribute' => 'id_evento',
                'value' => $model->evento->nombre_externo
            ],
        ],
    ]) ?>

</div>
