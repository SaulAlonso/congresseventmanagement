<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AsistenciaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Asistencias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="asistencia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', [
        'model' => $searchModel,
        'usuarios' => $usuarios,
        'eventos' => $eventos,
        ]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Registrar asistencia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_user',
                'value' => function($model, $key, $index, $column) {
                    return $model->user->name;
                },
            ],
            [
                'attribute' => 'id_evento',
                'value' => function($model, $key, $index, $column) {
                    //return $model->id_evento;
                    return $model->evento->nombre_externo;
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
