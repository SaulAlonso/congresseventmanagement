<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modelsEventosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nombre_externo') ?>

    <?= $form->field($model, 'nombre_interno') ?>

    <?= $form->field($model, 'emision') ?>

    <?= $form->field($model, 'descripcion') ?>

    <?php echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'contacto') ?>

    <?php // echo $form->field($model, 'id_tipo_evento') ?>

    <?php // echo $form->field($model, 'id_idioma') ?>

    <?php // echo $form->field($model, 'id_locacion') ?>

    <?php // echo $form->field($model, 'inscripcion_apertura') ?>

    <?php // echo $form->field($model, 'inscripcion_cierre') ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Buscar'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(\Yii::t('app', 'Refrescar'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
