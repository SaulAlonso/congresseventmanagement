<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Eventos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventos-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'nombre_externo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_interno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'emision')->textInput(['type' => 'number','min'=>2010]) ?>

    <?= $form->field($model, 'cupo_max')->textInput(['type' => 'number','min'=>0]) ?>

    <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_titular')->dropDownList(ArrayHelper::map($users, 'id', 'name')) ?>

    <?= $form->field($model, 'contacto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'moderador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'requisitos')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'id_tipo_evento')->dropDownList(ArrayHelper::map($tipos_de_eventos, 'id', 'nombre'), ['prompt' => \Yii::t('app', 'Seleccione uno')]) ?>

    <?= $form->field($model, 'id_idioma')->dropDownList(ArrayHelper::map($idiomas, 'id', 'nombre')) ?>

    <?= $form->field($model, 'id_locacion')->dropDownList(ArrayHelper::map($locaciones, 'id', 'nombre'), ['prompt' => \Yii::t('app', 'Seleccione uno')]) ?>

    <?= $form->field($model, 'inscripcion_apertura')->widget(
        'trntv\yii\datetime\DateTimeWidget',
        [
            'phpDatetimeFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                'allowInputToggle' => false,
                'sideBySide' => true,
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'auto'
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'inscripcion_cierre')->widget(
        'trntv\yii\datetime\DateTimeWidget',
        [
            'phpDatetimeFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                'allowInputToggle' => false,
                'sideBySide' => true,
                'widgetPositioning' => [
                    'horizontal' => 'auto',
                    'vertical' => 'auto'
                ]
            ]
        ]
    ) ?>

    <?= $form->field($model, 'foto')->fileInput() ?>

    <?= $form->field($model, 'estado')->dropDownList(['0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <?= $form->field($model, 'confirmado')->checkBox()?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ?  \Yii::t('app', 'Crear') : \Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
