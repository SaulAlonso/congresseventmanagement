<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Eventos */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Eventos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventos-view">

    <p>
        <?= Html::a(\Yii::t('app', 'Crear evento'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Confirma eliminar este registro?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'foto',
            'nombre_externo',
            'nombre_interno',
            'emision',
            [
                'attribute' => 'id_titular',
                'value' => $model->titular->name
            ],
            'cupo_max',
            'contacto',
            'moderador',
            'requisitos',
            [
                'attribute' => 'id_tipo_evento',
                'value' => $model->tipoEvento->nombre
            ],
            [
                'attribute' => 'id_idioma',
                'value' => $model->idioma->nombre
            ],
            [
                'attribute' => 'id_locacion',
                'value' => $model->locacion->nombre
            ],
            [
                'attribute' => 'estado',
                'value' => ($model->estado)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No')
            ],
            'inscripcion_apertura',
            'inscripcion_cierre',
        ],
    ]) ?>

</div>
