<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modelsEventosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Eventos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventos-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Crear evento'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre_externo',
            'emision',
            [
                'attribute' => 'id_tipo_evento',
                'value' => function($model, $key, $index, $column) {
                    return $model->tipoEvento->nombre;
                },
            ],
            /*[
                'attribute' => 'id_titular',
                'value' => function($model, $key, $index, $column) {
                    return $model->titular->name;
                },
            ],*/
            [
                'attribute' => 'id_locacion',
                'value' => function($model, $key, $index, $column) {
                    return $model->locacion->nombre;
                },
            ],
            'moderador',
            'cupo_max',
/*            [
                'attribute' => 'inscripcion_apertura',
                'value' => function($model) {
                    return date('Y-m-d',strtotime($model->inscripcion_apertura));
                },
            ],
            [
                'attribute' => 'inscripcion_cierre',
                'value' => function($model) {
                    return date('Y-m-d', strtotime($model->inscripcion_cierre));
                },
            ],*/
            [
                'attribute' => 'estado',
                'value' => function($model, $key, $index, $column) {
                    return ($model->estado)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No');
                },
            ],
	    [
		'attribute' => 'confirmado',
		'value' => function($model, $key, $index, $column) {
                    return ($model->confirmado)?\Yii::t('app', 'Sí'):\Yii::t('app','No');
                },
	    ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
