<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Eventos */

$this->title = \Yii::t('app', 'Crear evento');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Eventos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="eventos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'tipos_de_eventos' => $tipos_de_eventos,
        'idiomas' => $idiomas,
        'locaciones' => $locaciones,
        'users' => $users,
    ]) ?>

</div>
