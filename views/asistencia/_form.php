<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\Fechas;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $model app\models\eventos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="eventos-check-form" id="check_form">

    <?php $form = ActiveForm::begin(array(
    	'id'=>'talleres-form',
    	'action'=>Url::to(['actualizar']),
    	)
    );
    if(isset($msg))
        print_r('<div style="color:red;">'.($msg).'</div>');
    print_r('<fieldset><legend><h2>'.date("d M Y", strtotime($eventos[0]->fecha)).'</h2></legend>');
    for($i=0;$i<count($eventos);$i++){
        $disponibilidad = ($eventos[$i]->disponibilidad > 0)?$eventos[$i]->disponibilidad:'<label style="color:red;">'.\Yii::t('app','Lleno').'</label>';
    	$disabled = ($eventos[$i]->disponibilidad <= 0)?'disabled':'';

        $return ='<div class="'.(($i%2)?"table-active":"").'"><p><label>';
    	$return .= '<input type="checkbox" '.$disabled.' class="eventos" name="id_evento[]" value="' . $eventos[$i]->id . '"  >';
        $return .= '<span>    '.($eventos[$i]->nombre_externo).'</span></label><br>';
        $return .= ($eventos[$i]->contacto!='')?''.$eventos[$i]->contacto.'<br>':'';
        $return .= \Yii::t('app', 'Tipo').': '.($eventos[$i]->tipoEvento->nombre).'<br>';
        $return .= \Yii::t('app', 'Moderador').': '.($eventos[$i]->moderador).'<br>';
        $return .= \Yii::t('app', 'Disponibilidad').': '.$disponibilidad.'<br>';
        $return .= ($eventos[$i]->requisitos=='')?'':\Yii::t('app', 'Requisitos').': '.$eventos[$i]->requisitos.'<br>';
        $return .= Html::a(\Yii::t('app', 'Horarios'),FALSE,
            [
                'title' => '',
                'value' => Url::to(['info/detalle', 'id' => $eventos[$i]->id]),
                'class' => 'show-modal-button',
                'style' => 'color: #337ab7; cursor: pointer;',
            ]
        );
        $return .=' <div id="alert2'.$eventos[$i]->id.'" class="alertTxt"  style="color:red; display: none;">'.\Yii::t('app', '* Conflicto entre horarios.').'</div></p></div>';
    	$return .=(($i+1)<count($eventos) && $eventos[$i]->fecha!=$eventos[$i+1]->fecha)?'</fieldset><br><fieldset><legend>'.date("d M Y", strtotime($eventos[$i+1]->fecha)).'</legend>':'';
        print_r($return);
    }
    print_r('</fieldset>')
    ?>
    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Guardar'), ['class' => 'btn btn-primary','data' => [
                'confirm' => \Yii::t('app', 'Confirma el envío del formulario, una vez registrado no podrá realizar cambios?'),
                'method' => 'post'
        ],
]) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
