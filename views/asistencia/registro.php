<?php
use yii\helpers\Url;

$this->title = \Yii::t('app', 'Registro');
/* @var $this yii\web\View */
$this->registerJsFile('@web/js/script.js', ['depends' =>[\yii\web\JqueryAsset::className()]]);
?>
<div class="div-sections">
	<img class="img cimps-logo" src="<?=\Yii::$app->params['logoEmision']?>"/>
	<img class="img cimat-logo" src="img/cimat-logo.png"/>
	<img class="img pnpc-logo" src="img/pnpc.png"/>
</div>
<br>
<div class="div-sections">	
	<h1>CIMPS <?=\Yii::$app->params['emision']?> <?=\Yii::t('app', 'Eventos')?></h1>
	<p>
    <?= $this->render('_form', [
        'model' => $model,
        'eventos' => $eventos,
        'asistencia' => $asistencia,
        'msg' => (isset($msg))?$msg:'',
    ]) ?>
	</p>
</div>
