<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HorariosPorEmisionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Horarios por emision');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horarios-por-emision-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Crear horario'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'hora_inicio',
            'hora_fin',
            'emision',
            [
                'attribute' => 'hora_de_registro',
                'value' => function($model, $key, $index, $column) {
            	    return ($model->hora_de_registro)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No');
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
