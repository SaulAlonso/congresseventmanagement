<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HorariosPorEmision */

$this->title = \Yii::t('app', 'Actualizar horarios por emision').': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Horarios por emision'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Actualizar');
?>
<div class="horarios-por-emision-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
