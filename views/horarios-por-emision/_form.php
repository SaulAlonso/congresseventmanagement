<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HorariosPorEmision */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="horarios-por-emision-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'hora_inicio')->textInput() ?>

    <?= $form->field($model, 'hora_fin')->textInput() ?>

    <?= $form->field($model, 'emision')->textInput() ?>

    <?= $form->field($model, 'hora_de_registro')->dropDownList(['0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
