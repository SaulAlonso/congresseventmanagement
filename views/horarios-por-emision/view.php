<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HorariosPorEmision */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Horarios por emision'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horarios-por-emision-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Crear horario'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', [\Yii::t('app', 'Eliminar'), 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Confirmas eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'hora_inicio',
            'hora_fin',
            'emision',
            [
                'attribute' => 'hora_de_registro',
                'value' => ($model->hora_de_registro)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No')
    	    ],
        ],
    ]) ?>

</div>
