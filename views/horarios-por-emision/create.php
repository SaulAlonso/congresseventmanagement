<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\HorariosPorEmision */

$this->title = \Yii::t('app', 'Crear horario por emision');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Horarios por emision'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="horarios-por-emision-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
