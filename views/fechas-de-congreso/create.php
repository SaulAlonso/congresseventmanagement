<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FechasDeCongreso */

$this->title = \Yii::t('app', 'Crear fecha de congreso');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Fechas de congreso'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-de-congreso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
