<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FechasDeCongreso */

$this->title = \Yii::t('app', 'Actualizar fecha de congreso: '). $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Fechas de congreso'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fechas-de-congreso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
