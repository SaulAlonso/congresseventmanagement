<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FechasDeCongreso */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Fechas de congreso'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-de-congreso-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(\Yii::t('app', 'Crear fecha de congreso'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Confirma eliminar este registro?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'fecha',
            'emision',
            [
        	'attribute' => 'horario_de_registro',
                'value' => ($model->horario_de_registro)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No')
            ],
        ],
    ]) ?>

</div>
