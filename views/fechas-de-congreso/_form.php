<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FechasDeCongreso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-de-congreso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha')->widget(
        'trntv\yii\datetime\DateTimeWidget',
            [
        	'phpDatetimeFormat' => 'yyyy-MM-dd',
        	'clientOptions' => [
	    	    'allowInputToggle' => false,
            	    'sideBySide' => true,
            	    'widgetPositioning' => [
    	        	'horizontal' => 'auto',
                	'vertical' => 'auto'
            	    ]
        	]
            ]
        ) ?>

    <?= $form->field($model, 'emision')->textInput() ?>

    <?= $form->field($model, 'horario_de_registro')->dropDownList(['0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? \Yii::t('app', 'Crear') : \Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
