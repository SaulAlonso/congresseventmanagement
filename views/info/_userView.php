<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            'city',
            'country',
        ],
    ]) ?>
</div>
