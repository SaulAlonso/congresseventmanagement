<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Fechas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-form">
    <?php
    Yii::$app->formatter->locale = 'es-ES';
    $formatter = \Yii::$app->formatter;
    $result = '';
    $current ='';
    foreach ($eventos as $evento) {
        $result .= '<fieldset><legend><p>'.$evento->nombre_externo.'</p></legend>';
        $result .= ($evento->contacto!='')?ucwords($evento->contacto).'<br>':'';
        $disponibilidad = ($disponibilidad = $evento->disponibilidad<=0)?\Yii::t('app', 'Lleno'):$evento->disponibilidad;
        $result .=  \Yii::t('app', 'Tipo').': '.ucwords($evento->tipoEvento->nombre).'<br>';
        $result .= \Yii::t('app', 'Disponibilidad').'</label>: '.$disponibilidad.'<br>';
        
        foreach ($evento->fechas as $fecha) {
            if($current!=$fecha->fecha){
                $current = $fecha->fecha;
                $result .= '<br>';
		$result .= '<strong>'.$formatter->asDate($fecha->fecha, 'long').'</strong><br>';
                $result .= '<h4> '.\Yii::t('app','Horario').'</h4>';
            }
            $result .= $fecha->horario->hora_inicio.' - '.$fecha->horario->hora_fin.'<br>';
        }
        $result .= '</fieldset>';
        $result .= '<br><br>';
    }
    print_r($result);
    ?>
</div>
