<?php

use yii\helpers\Html;
use yii\helpers\Url;
use Da\QrCode\QrCode;
/* @var $this yii\web\View */


$qrCode = (new QrCode(\Yii::$app->user->identity->id))
    ->setSize(250)
    ->setMargin(5)
    ->useForegroundColor(51, 153, 255);

$qrCode->writeFile('img/qr/QR'.\Yii::$app->user->identity->id.'.png'); 

?> 
<div class="div-sections">
	<img class="img cimps-logo" src="<?=\Yii::$app->params['logoEmision']?>"/>
	<img class="img cimat-logo" src="img/cimat-logo.png"/>
	<img class="img pnpc-logo" src="img/pnpc.png"/>
</div>
<br>
<div class="div-sections">	
	<h2><?=\Yii::t('app', 'Datos personales')?></h2>
	<hr>
	<div style="width: 60%;float: left;">
	    <?= $this->render('_userView', [
	        'model' => $user,
	    ]) ?>
    </div>
    <div style="width: 40%;float: left;">
	    <?='<img style="width: 40%;" src="img/qr/QR'.\Yii::$app->user->identity->id.'.png"/>'?>
    </div>
</div>
<div class="div-sections">
	<h2><?=\Yii::t('app', 'Eventos seleccionados')?>
	<?php if (isset($icon_download)){ ?>
	    <a href='<?= Url::to(['info/pdf']) ?>'><img title='Descargar PDF' style='width: 5%;' src='img/PDF-Icon-Download.png'/></a>
	<?php } ?>
	</h2>
	<hr>
	<p>
    <?= $this->render('_carga', [
        'eventos' => $eventos,
    ]) ?>
	</p>
</div>
