<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FechasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::t('app', 'Horarios asignados');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel ,'fechas_de_congreso' => $fechas_de_congreso, 'eventos' =>$eventos, 'horarios' => $horarios]); ?>

    <p>
        <?= Html::a(\Yii::t('app', 'Asingar horario a evento'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            [
                'attribute' => 'evento',
                'value' => function($model, $key, $index, $column) {
            	    //return $model->id_evento;
                    return $model->evento->nombre_externo;
                },
            ],
            'fecha',
            [
                'attribute' => 'id_horario',
                'value' => function($model, $key, $index, $column) {
                    //return $model->id_evento;
                    return date("h:i", strtotime($model->horario->hora_inicio))." - ".date("h:i A", strtotime($model->horario->hora_fin));
                },
            ],
            [
                'attribute' => 'eventos_en_paralelo',
                'value' => function($model){
                    return ($model->eventos_en_paralelo==true)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No');
                },
            ],
            [
                'attribute' => 'estado',
                'value' => function($model){
                    return ($model->estado==true)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No');
                },
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
</div>
