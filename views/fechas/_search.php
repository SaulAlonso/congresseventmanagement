<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Eventos;
/* @var $this yii\web\View */
/* @var $model app\models\FechasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_evento')->dropDownList(ArrayHelper::map($eventos, 'id','nombre_externo'), ['prompt'=>\Yii::t('app', 'Seleccione...')]) ?>

    <?= $form->field($model, 'fecha')
	->dropDownList(ArrayHelper::map($fechas_de_congreso, 'fecha','fecha'), ['prompt'=>\Yii::t('app', 'Seleccione fecha...')]) 
	?>

    <?= $form->field($model, 'id_horario')->dropDownList(ArrayHelper::map($horarios, 'id','hora_inicio'), ['prompt'=>\Yii::t('app', 'Seleccione...')]) ?>

    <?php // $form->field($model, 'hora_inicio') ?>

    <?php // $form->field($model, 'hora_fin') ?>

    <?php // echo $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'descripcion') ?>

    <div class="form-group">
        <?= Html::submitButton(\Yii::t('app', 'Buscar'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(\Yii::t('app', 'Refrescar'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
