<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Fechas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_evento')->dropDownList(ArrayHelper::map($eventos,'id', 'nombre_externo'), ['prompt'=>\Yii::t('app', 'Seleccione...')]) ?>

    <?= $form->field($model, 'fecha')->dropDownList(ArrayHelper::map($fechas_de_congreso,'fecha', 'fecha'), ['prompt'=>\Yii::t('app', 'Seleccione fecha...')]) ?>

    <?= $form->field($model, 'id_horario')->dropDownList(ArrayHelper::map($horarios, 'id',
    function($model){
	return $model['hora_inicio'].' - '.$model['hora_fin'];
    }), ['prompt'=>\Yii::t('app', 'Seleccione...')]) ?>

    <?= $form->field($model, 'eventos_en_paralelo')->dropDownList(['1'=>\Yii::t('app', 'Sí'),'0'=>\Yii::t('app', 'No')]) ?>

    <?= $form->field($model, 'estado')->dropDownList(['0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? \Yii::t('app', 'Crear') : \Yii::t('app', 'Actualizar'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
