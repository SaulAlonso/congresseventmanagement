<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechas */

$this->title = \Yii::t('app', 'Actualizar fecha').': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Fechas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = \Yii::t('app', 'Actualizar');
?>
<div class="fechas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fechas_de_congreso' => $fechas_de_congreso,
        'eventos' => $eventos,
        'horarios' => $horarios,
    ]) ?>

</div>
