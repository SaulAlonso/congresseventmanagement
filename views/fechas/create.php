<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Fechas */

$this->title = \Yii::t('app', 'Asingar horario a evento');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Horarios asignados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
        'fechas_de_congreso' => $fechas_de_congreso,
        'eventos' => $eventos,
        'horarios' => $horarios,
    ]) ?>

</div>
