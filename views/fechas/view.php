<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fechas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Horarios asignados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-view">

    <p>
        <?= Html::a(\Yii::t('app', 'Asignar horario a evento'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(\Yii::t('app', 'Actualizar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(\Yii::t('app', 'Borrar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => \Yii::t('app', 'Confirma eliminar este registro?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id_evento',
                'value' => $model->evento->nombre_externo
            ],
            'fecha',
            [
                'attribute' => 'id_horario',
                'value' => date("h:i", strtotime($model->horario->hora_inicio))." - ".date("h:i A", strtotime($model->horario->hora_fin))
            ],
            [
                'attribute' => 'eventos_en_paralelo',
                'value' => ($model->eventos_en_paralelo)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No')
            ],
            [
                'attribute' => 'estado',
                'value' => ($model->estado)?\Yii::t('app', 'Sí'):\Yii::t('app', 'No')
            ],
        ],
    ]) ?>

</div>
