<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FechasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile("@web/js/mail.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = \Yii::t('app', 'Administrador de envio de correos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <fieldset>
	<legend> Prueba de envio de correo</legend>
	    <table>
	    <tr><td>
    		<label for="correoTest">Correo electr&oacute;nico</label><input id="correoTest" name="correoTest"/>
    		</td><td>
    		<div id="loading_gif_test" style="display:none;"><img src="img/ajax-loader(2).gif" ></div><br>
    		</td></tr>
    	    <tr><td>
        	    <button id="mailTestButton" name="mailTestButton"><?= \Yii::t('app', 'Enviar correo de prueba'); ?></button>
	    </td></tr>
	</table>
    </fieldset>
    <br>
    <fieldset>
	<legend> Envio masivo de correo</legend>
	    <table>
	    <tr><td>
		<label for="masiveMailSubject">Asunto de correo masivo</label><input id="masiveMailSubject" name="masiveMailSubject"/><br>
		</td></tr>
    		<tr><td>
    		<label for="masiveMailTemplate">Plantilla de correo masivo</label><br>
		</td></tr>
    		<tr><td>
    		*** El envio de correos masivo debe ser autorizado por el Dr. Jezreel, él es el que proporciona el mensaje que se enviará.<br>
		*** Se deve notificar a Facundo en CIMAT Guanajuato para que habilite una excepción y no sea bloqueada la cuenta por el envio masivo.<br>
		*** Los correos los toma de una tabla en base de datos llamada "correos para envio masivo", se deve revisar que sean los correctos.<br>
		*** Puedes enviar una prueba del mensaje a enviar en la sección de arriba.<br>
		*** La etiqueta \Yii::t('app', ' **TEXTO**') en la plantilla es para la traducción, se debe de proporcionar la tradución en los archivos correspondientes, o no realizará su funcionalidad.<br>
		</td></tr>
    		<tr><td>
    		<textarea id="masiveMailTemplate" name="masiveMailTemplate" rows="15" cols="100">
    		<?php
    		$fileURL = "../mail/layouts/masivo.php";
    		$myfile = file_get_contents($fileURL, true);
    		echo $myfile;
    		?>
    		</textarea>
    		</td></tr>
    		<tr><td>
    		<div id="loading_gif_masivo" style="display:none;"><img src="img/ajax-loader(2).gif" ></div><br>
		</td></tr>
    		<tr><td>
	    	    <button id="updateTemplate" name="updateTemplate"><?= \Yii::t('app', 'Actualizar plantilla de correo masivo'); ?></button><br>
		</td></tr>
    		<tr><td>
    		    <button id="mailMassiveButton" name="mailMassiveButton"><?= \Yii::t('app', 'Enviar correo masivo'); ?></button>
		</td></tr>
    	    </table>
    </fieldset>
</div>
