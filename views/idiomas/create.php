<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Idiomas */

$this->title = \Yii::t('app', 'Crear idioma');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Idiomas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="idiomas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
