<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CertificadosEntregadosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="certificados-entregados-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'username') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'accept')->dropDownList([''=>\Yii::t('app', 'Seleccione...'), '0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <?= $form->field($model, 'gaffete')->dropDownList([''=>\Yii::t('app', 'Seleccione...'), '0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <?= $form->field($model, 'certificado_enviado')->dropDownList([''=>\Yii::t('app', 'Seleccione...'), '0'=>\Yii::t('app', 'No'),'1'=>\Yii::t('app', 'Sí')]) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['name' => 'certificates-filter','id'=>'certificates-filter', 'class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
        <?= Html::button('Send certificates', ['name' => 'certificates-mail','id'=>'certificates-mail', 'class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
