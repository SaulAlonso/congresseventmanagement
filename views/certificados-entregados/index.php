<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CertificadosEntregadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile("@web/js/certificados.js", ['depends' => [\yii\web\JqueryAsset::className()]]);

$this->title = 'Certificados Entregados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="certificados-entregados-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'username',
            'email:text',
            'accept',
            'gaffete',
            'certificado_enviado',
            
	    ['class' => 'yii\grid\ActionColumn',
		'template' => '{view}{mail}',
                'buttons'=>[
                    'view'=>function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open btn-xs ']);
                    },
                    'mail'=>function ($url) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-envelope btn-xs ']);
                    },
                ],
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
