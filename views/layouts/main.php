<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use app\models\Users;
use app\models\Asistencia;

AppAsset::register($this);
$registred = (isset(\Yii::$app->user->identity) && count(Asistencia::find()->where(['id_user'=>\Yii::$app->user->identity->id])->all())>0)?true:false;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Saul Alonso Ibarra Luevano">
    <meta name="keywords" content="CIMPS, Mejora de Procesos, SPI, MPS, Conference, process improvement, CIMAT, Software, Software engineering">
    <script src="https://use.fontawesome.com/c03a334941.js"></script>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<div id="modal-div" style="display: none;">
    <div style=" background-color: #E6E6E6; opacity: 0.4; height: 650px; margin-top: 0; position: fixed; width: 99%; right: 0; z-index: 1100;">
    </div>
    <img style="margin:25% 0 0 50%; position: fixed; z-index: 1200;" src="img/ajax-loader.gif">
</div>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => \Yii::t('app', 'Registro de eventos CIMPS').' '.\Yii::$app->params['emision'],
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => \Yii::t('app', 'Cambiar idioma'),
                'items' => [
                    [
                        'label' => \Yii::t('app', 'Español'), 
                        'url' => Url::to(['lang/change-lang', 'lang' => 'es'])
                    ],
                    [
                        'label' => \Yii::t('app', 'Inglés'), 
                        'url' => Url::to(['lang/change-lang', 'lang' => 'en'])
                    ],
                ],
           ],
           [
        	'label'=> \Yii::t('app', 'Iniciar sesión'),
    		'url'=>'http://cimps.cimat.mx/registro/index.php/auth',
    		'visible'=>(isset(\Yii::$app->user->identity))?false:true,
           ],
        ],
    ]);
    if(isset(\Yii::$app->user->identity))
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => \Yii::t('app', 'Programa General'), 'url' => ['/public'],
            ],
            ['label' => \Yii::t('app', 'Registro'), 'url' => ['/info'], 'visible' => (isset(\Yii::$app->user->identity->grupo) && \Yii::$app->user->identity->grupo->id==Users::PONENTE)?false:true,],
            //['label' => \Yii::t('app', 'Descargar PDF'), 'url' => ['/info/pdf'], 'visible' => $registred],
            [ 'label' => \Yii::t('app', 'Talleres/Ponencias'), 'url' => ['/info/ponente'], 'visible' => (isset(\Yii::$app->user->identity->grupo) && \Yii::$app->user->identity->grupo->id==Users::PONENTE)?true:false,
            ],
            [ 'label' => \Yii::t('app', 'Menú de gestión'), 'icon' => 'list-alt', 'items' =>[
                ['label' => \Yii::t('app', 'Fechas de congreso'), 'url' => ['/fechas-de-congreso']],
                ['label' => \Yii::t('app', 'Eventos'), 'url' => ['/eventos']],
                ['label' => \Yii::t('app', 'Locaciones'), 'url' => ['/locaciones']],
                ['label' => \Yii::t('app', 'Horarios disponibles'), 'url' => ['/horarios-por-emision']],
                ['label' => \Yii::t('app', 'Asignar horarios a eventos'), 'url' => ['/fechas']],
                ['label' => \Yii::t('app', 'Idiomas'), 'url' => ['/idiomas']],
                ['label' => \Yii::t('app', 'Tipos de eventos'), 'url' => ['/tipos']],
                ['label' => \Yii::t('app', 'Administrador de asistencia'), 'url' => ['/administrador']],
                ['label' => \Yii::t('app', 'Administrador de envio de correos'), 'url' => ['/mail']],
                ['label' => \Yii::t('app', 'Envio de constancias'), 'url' => ['/certificados-entregados']],
                ],
                'visible' => (isset(\Yii::$app->user->identity->grupo) && \Yii::$app->user->identity->grupo->id==Users::ADMIN)?true:false,
            ],
           ],
    ]);
    NavBar::end();
    ?>
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?=\Yii::t('app', 'Congreso Internacional en Mejora de Procesos de Software')?> <?= \Yii::$app->params['emision'] ?></p>
    </div>
</footer>
<?php
Modal::begin([
    'header' => '<span id="modal_header_title"></span>',
    'headerOptions' => ['id' => 'modal_header'],
    'id' => 'modal',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modal_content'>
	<div>
	    <div style='width:100%;'> <img class='img-responsive' style='margin:auto;' src='".\Yii::$app->params['logoEmision']."'>"
	    ."</div>
    	    <div style='text-align:center'>".
    		"<h3><strong>".\Yii::t('app', 'Importante!')."</strong> ".
    		\Yii::t('app', 'Ahora puedes inscribirte a más de dos talleres y sesiones, revisa la disponibilidad de horario y el cupo limite.').
    		((!isset(\Yii::$app->user->identity))?'<br>'.\Yii::t('app', 'Recuerda iniciar sesión para inscribirte a conferencias, sesiones y talleres.'):'').
    	    "</h3></div>
	</div>
    </div>";
Modal::end();
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>