<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Locaciones */

$this->title = \Yii::t('app', 'Crear locación');
$this->params['breadcrumbs'][] = ['label' => \Yii::t('app', 'Locaciones'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
