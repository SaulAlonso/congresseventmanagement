<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * CertificadosEntregadosController implements the CRUD actions for CertificadosEntregados model.
 */
class CertificadosEntregadosController extends Controller
{
     public function init()
     {
         if(!isset(\Yii::$app->user->identity))
             $this->redirect(Url::to(['/public']));
         \Yii::$app->language = \Yii::$app->session['lang'];
     }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CertificadosEntregados models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $send_mail = \Yii::$app->request->get('send_mail');
        if($send_mail){
    	    try{
    	        $users = $dataProvider->getModels();
		foreach ($users as $user) {
		    $result = $this->sendUserMail($user);
	        }
	    } catch (Exception $e){
	    	return Json::encode(['Success'=>false, 'error'=>$e]);
	    }
    	}
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CertificadosEntregados model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
	$user =  $this->findModel($id);
	
	$url_user_certificate = $this->setUserCertificate($user);
	if(file_exists($url_user_certificate))
	{
    	    return $this->render('view', [
        	'url_certificado' => $url_user_certificate,
    	    ]);
    	}
        return $this->render('view', [
    	    'url_certificado' => '',
    	]);
    }


    /**
     * Sends a single Certificados mail.
     * @param integer $id
     * @return json
     */
    public function actionMail($id)
    {
	$user =  $this->findModel($id);
	$result = $this->sendUserMail($user);
        return Json::encode(['Success'=>$result]);
    }


    /**
    * Send mail to just one user
    ** insede of the CertificadosEntregados class
    ** @param $user User
    ** @return boolean
    */
    private function sendUserMail($user)
    {
        try{
	    $url_user_certificate = $this->setUserCertificate($user);
	    if(file_exists($url_user_certificate))
	    {
		\Yii::$app->mailer->compose('layouts/certificado.php')
            	    ->setFrom(\Yii::$app->params['adminEmail'])
	    	    ->setTo($user->email)
	    	    ->setSubject('Certificado CIMPS 2018')
	    	    ->attach($url_user_certificate)
	    	    ->send();
	    	//se actualiza el estatus de certificado enviado
	        $user->certificado_enviado = True;
	        $user->save();
    	    }
    	    return true;
	} catch (Exception $e){}
        return false;
    }

    /**
    ** checks if the user certificate file exists and return certificate url
    ** @param $user Users
    ** @return String
    */
    private function setUserCertificate($user)
    {
	$url_user_certificate = "img/certificados/CIMPS_".\Yii::$app->params['emision']."_".$user->id.".jpg";
	if(!file_exists($url_user_certificate))
	{
	    //Se genera la imagen del certificado en el servidor
    	    $url_template_certificate = "img/reconocimiento_cimps.jpg";
    	    // Create image from existing file
    	    $jpg_image = imagecreatefromjpeg($url_template_certificate);

    	    // pick color for the text
    	    $color= imagecolorallocate($jpg_image, 0, 0, 0);

    	    // Set font to the text
    	    $font_path = 'fonts/ariali.ttf';

    	    // Set text to be write on image
    	    $name = $user->name;
        
    	    // set text position
    	    $name_length = strlen($name);
    	    // a partir de la mitad de la imagen (600), se retrocede tantos lugares como lentras tenga la mitad del nombre,
    	    // considerando que con ese tamaño y fuente le caben 26 letras a la mitad de la imagen
    	    $name_position = 600 - ((600 / 26) * ($name_length / 2 )); 

    	    // Write text on image
    	    imagettftext($jpg_image, 40, 0, $name_position, 800, $color, $font_path, $name);

    	    // tell the browser that the content is an image
    	    header('Content-type: image/jpeg');

    	    // output image to the browser
    	    imagejpeg($jpg_image, $url_user_certificate);

    	    // delete the image resource 
    	    imagedestroy ($jpg_image);
	}
	return $url_user_certificate;
    }

    /**
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CertificadosEntregados the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UsersSearch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested user does not exist.');
        }
    }
}
