<?php
/*
*** Controlador para asignar horarios y fechas a eventos registrados
** se necesita instanciar eventos en horarios para que se muestren en el programa
*/
namespace app\controllers;

use Yii;
use app\models\Fechas;
use app\models\FechasSearch;
use app\models\Eventos;
use app\models\Logs;
use app\models\HorariosPorEmision;
use app\models\FechasDeCongreso;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FechasController implements the CRUD actions for Fechas model.
 */
class FechasController extends Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Fechas models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $searchModel = new FechasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $horarios = HorariosPorEmision::find()
            ->where(['emision'=>\Yii::$app->params['emision']])
            ->orderBy([
                'hora_inicio' => 'SHORT_ASC',
            ])->all();
        $eventos = Eventos::find()
            ->andWhere(['and', ['emision'=>\Yii::$app->params['emision']]])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        $fechas_de_congreso = FechasDeCongreso::find()
    	    ->where(['emision'=>\Yii::$app->params['emision']])
    	    ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'fechas_de_congreso' => $fechas_de_congreso,
            'dataProvider' => $dataProvider,
            'eventos' => $eventos,
            'horarios' => $horarios,
        ]);
    }

    /**
     * Displays a single Fechas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fechas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fechas();
        $horarios = HorariosPorEmision::find()
            ->where(['emision'=>\Yii::$app->params['emision']])
            ->orderBy([
                'hora_inicio' => 'SHORT_ASC',
            ])->all();
        $eventos = Eventos::find()
            ->andWhere(['and', ['emision'=>\Yii::$app->params['emision']]])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        $fechas_de_congreso = FechasDeCongreso::find()
    	    ->where(['emision'=>\Yii::$app->params['emision']])
    	    ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'fechas_de_congreso' => $fechas_de_congreso,
                'eventos' => $eventos,
                'horarios' => $horarios,
                'result' => $model->save(),
                'post' => Yii::$app->request->post(),
            ]);
        }
    }

    /**
     * Updates an existing Fechas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $horarios = HorariosPorEmision::find()
            ->where(['emision'=>\Yii::$app->params['emision']])
            ->orderBy([
                'hora_inicio' => 'SHORT_ASC',
            ])->all();
        $eventos = Eventos::find()
            ->andWhere(['and', ['emision'=>\Yii::$app->params['emision']]])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        $fechas_de_congreso = FechasDeCongreso::find()
    	    ->where(['emision'=>\Yii::$app->params['emision']])
    	    ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'fechas_de_congreso' => $fechas_de_congreso,
                'eventos' => $eventos,
                'horarios' => $horarios,
            ]);
        }
    }

    /**
     * Deletes an existing Fechas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Fechas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Fechas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Fechas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'La página solicitada no existe.'));
        }
    }
    
    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->name) and \Yii::$app->user->name!=null)?\Yii::$app->user->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
