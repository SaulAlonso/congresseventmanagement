<?php
/*
*** Controlador del admin para eliminar/editar/agregar registros de usuarios a eventos
*/

namespace app\controllers;

use Yii;
use app\models\Asistencia;
use app\models\AsistenciaSearch;
use app\models\Eventos;
use app\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class AdministradorController extends Controller
{
    public function init()
    {
	if(!isset(\Yii::$app->user->identity))
    	    $this->redirect(Url::to(['/public']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Asistencia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $eventos = Eventos::find()
    	    ->where(['emision' => \Yii::$app->params['emision']])
    	    ->all();
        $usuarios = Users::find()->all();
        $searchModel = new AsistenciaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'usuarios' =>$usuarios,
            'eventos' => $eventos,
        ]);
    }

    /**
     * Displays a single Asistencia model.
     * @param integer $id_user
     * @param integer $id_evento
     * @return mixed
     */
    public function actionView($id_user, $id_evento)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_user, $id_evento),
        ]);
    }

    /**
     * Creates a new Asistencia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Asistencia();
        $eventos = Eventos::find()
    	    ->where(['emision' => \Yii::$app->params['emision']])
    	    ->all();
        $usuarios = Users::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_user' => $model->id_user, 'id_evento' => $model->id_evento]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'usuarios' =>$usuarios,
                'eventos' => $eventos,
            ]);
        }
    }

    /**
     * Updates an existing Asistencia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_user
     * @param integer $id_evento
     * @return mixed
     */
    public function actionUpdate($id_user, $id_evento)
    {
        $model = $this->findModel($id_user, $id_evento);
        $eventos = Eventos::find()
    	    ->where(['emision' => \Yii::$app->params['emision']])
    	    ->all();
        $usuarios = Users::find()->all();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 
        	'id_user' => $model->id_user, 
        	'id_evento' => $model->id_evento,
                'usuarios' =>$usuarios,
                'eventos' => $eventos,
    	    ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'usuarios' =>$usuarios,
                'eventos' => $eventos,
            ]);
        }
    }

    /**
     * Deletes an existing Asistencia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_user
     * @param integer $id_evento
     * @return mixed
     */
    public function actionDelete($id_user, $id_evento)
    {
        $this->findModel($id_user, $id_evento)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Asistencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_user
     * @param integer $id_evento
     * @return Asistencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_user, $id_evento)
    {
        if (($model = Asistencia::findOne(['id_user' => $id_user, 'id_evento' => $id_evento])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
