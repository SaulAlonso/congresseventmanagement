<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Logs;
use app\models\Users;

class SiteController extends Controller
{
    public function init()
    {
        $session = \Yii::$app->session;
        $session->open();
        \Yii::$app->language = $session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if(isset($_GET['correo']) and isset($_GET['key'])){
            $this->redirect(Url::to(['site/login', 'correo'=>$_GET['correo'], 'key'=>$_GET['key']]));
        }else
            $this->redirect(Url::to(['/public']));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin($correo, $key)
    {
        if(isset($correo) && isset($key))
        {
    	    if(md5(\Yii::$app->params['key'].$correo)==$key)
    	    {
        	$identity = Users::findOne(['email'=>$correo]);
        	if(count($identity)>0)
        	{
    	            \Yii::$app->user->login($identity);
    	            $this->redirect(Url::to(['/asistencia']));
        	}else
            	    $this->redirect(Url::to(['/public']));
    	    }else
        	$this->redirect(Url::to(['/public']));
        }else
    	    $this->redirect(URL::to(['/public']));
    }
    

    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->name) and \Yii::$app->user->name!=null)?\Yii::$app->user->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
