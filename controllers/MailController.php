<?php

namespace app\controllers;

use yii\helpers\Json;
use yii\helpers\Url;
use app\models\Fechas;
use app\models\Users;
use app\models\Logs;
use app\models\CorreoMasivo;

class MailController extends \yii\web\Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(['/public']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
    * Lists all Fechas models.
    * @return mixed
    */
    public function actionIndex()
    {
       if(!isset(\Yii::$app->user->identity))
           $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
       return $this->render('index');
    }

    public function actionTest()
    {
       if(!isset(\Yii::$app->user->identity))
    	    return Json::encode(['Success'=>false, 'error'=>'Logging needed.']);
        $correo = \Yii::$app->request->post('correo');
        if($correo){
    	    try{
	    	\Yii::$app->mailer->compose('layouts/masivo.php')
		->setFrom(\Yii::$app->params['adminEmail'])
    		->setTo([$correo])
    		->setSubject('Email Test sent from Yii2-Swiftmailer')
    		->send();
    		return Json::encode(['Success'=>true]);
    	    } catch (Exception $e){
		return Json::encode(['Success'=>false, 'error'=>$e]);
    	    }
        }
    }
    
    public function actionUpdatetemplate()
    {
       if(!isset(\Yii::$app->user->identity))
    	    return Json::encode(['Success'=>false, 'error'=>'Logging needed.']);
        $plantilla = \Yii::$app->request->post('plantilla');
        $authorized = \Yii::$app->request->post('authorized');
        if($authorized){
    	    try{
    		$fileURL = "../mail/layouts/masivo.php";
		$myfile = fopen($fileURL, "w") or die("Unable to open file!");
		fwrite($myfile, $plantilla);
    		return Json::encode(['Success'=>true]);
    	    } catch (Exception $e){
		return Json::encode(['Success'=>false, 'error'=>$e]);
    	    }
        }
	return Json::encode(['Success'=>false, 'error'=>'Unauthorized change.']);
    }
    
    public function actionMasivo()
    {
        if(!isset(\Yii::$app->user->identity))
    	    return Json::encode(['Success'=>false, 'error'=>'Logging needed.']);
        $authorized = \Yii::$app->request->post('authorized');
	$subject = \Yii::$app->request->post('subject');
        if($authorized){
            try{
                $correos = CorreoMasivo::find()->all();
        	$counter = 0;
        	$emailsList = array();
		foreach ($correos as $correo) {
	    	    try{
	    		\Yii::$app->mailer->compose('layouts/masivo.php')
       			->setFrom(\Yii::$app->params['adminEmail'])
        		->setTo($correo->correo)
        		->setSubject($subject)
    			->send();
    		    } catch (Exception $e){
    			$e = true;
        	    }
    		    array_push($emailsList,$correo->correo);
        	    $counter += 1;
    		}
    		return Json::encode(['Success'=>true, 'emailsSended'=>$counter,'emailsList'=>$emailsList]);
    	    } catch (Exception $e){
    		return Json::encode(['Success'=>false, 'error'=>$e]);
    	    }
        }
        return Json::encode(['Success'=>false, 'error'=>'Authorizing parameter is missing!']);
    }
    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = 'Navegador';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = 'Envio de correos masivo';
        $log->save();
        return true;
    }
}
