<?php
/*
*** Controlador para visualizar y descargar el listado de talleres registrados por usuario
*/
namespace app\controllers;

use Yii;
use mPDF;
use app\models\Asistencia;
use app\models\Fechas;
use app\models\FechasSearch;
use app\models\Eventos;
use app\models\Users;
use app\models\Logs;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

class InfoController extends \yii\web\Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    public function actionIndex()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $id_user = \Yii::$app->user->identity->id;
        $user = Users::findOne(['id'=>$id_user]);
        $eventos = Eventos::find()
            ->leftJoin('asistentes_por_fecha', 'asistentes_por_fecha.id_evento = eventos.id')
            ->where(['asistentes_por_fecha.id_user' => $id_user])
            ->andWhere(['and', ['emision'=>\Yii::$app->params['emision']]])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])
            ->all();
        return $this->render('viewPublico',[
            'user'=>$user,
            'eventos' => $eventos,
            'icon_download' => false,
        ]);
    }

    public function actionPdf()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $id_user = \Yii::$app->user->identity->id;
        $user = Users::findOne(['id'=>$id_user]);
        $mPDF = new mPDF();
        $eventos = Eventos::find()
            ->leftJoin('asistentes_por_fecha', 'asistentes_por_fecha.id_evento = eventos.id')
            ->where(['asistentes_por_fecha.id_user' => $id_user])
            ->andWhere(['and', ['emision'=>\Yii::$app->params['emision']]])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])
            ->all();
        $mPDF->writeHTML($this->render('viewPublico',[
            'user'=>$user,
            'eventos' => $eventos,
        ]));
        $mPDF->Output('CIMPS_'.\Yii::$app->params['emision'].'_'.$user->username.'.pdf', 'D');
    }

    // Para llevar registro en log
    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->identity->name) and \Yii::$app->user->identity->name!=null)?\Yii::$app->user->identity->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
