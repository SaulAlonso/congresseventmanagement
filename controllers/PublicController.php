<?php
namespace app\controllers;

use Yii;
use mPDF;
use app\models\Asistencia;
use app\models\Fechas;
use app\models\Eventos;
use app\models\Users;
use app\models\Logs;
use app\models\HorariosPorEmision;
use app\models\FechasDeCongreso;
use app\models\Locaciones;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class PublicController extends \yii\web\Controller
{
    public function init()
    {
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    public function actionPrograma_pdf()
    {
	$mPDF = new mPDF();
    	//return $this-> getProgram("programa_pdf");
        $mPDF->writeHTML($this-> getProgram("programa_pdf"));
        $mPDF->Output('CIMPS_'.\Yii::$app->params['emision'].'.pdf', 'D');
     }

    public function actionIndex()
    {
    	return $this-> getProgram("index");
    }

    public function getProgram($template)
    {
	$fechas = FechasDeCongreso::find()
	    ->where(['emision'=>\Yii::$app->params['emision']])
            ->orderBy([
                'fecha' => 'SHORT_ASC',
            ])->all();

        $locaciones = Locaciones::find()
            ->where(['emision'=>\Yii::$app->params['emision']])
            ->all();
    //Se obtienen los eventos y los horarios correspondientes a cada dia del congreso
    //**** Eventos dia 1 ****//
	if(isset($fechas[0])){
    	    $eventosdia1 = $this-> getDateEvents(
    		$fechas[0]->fecha //manda la fecha del primer dia
    		, $fechas[0]->horario_de_registro //Manda si el dia tiene horario para registro de asistentes
    		);
    	    $horariosquery = HorariosPorEmision::find()
        	->where(['emision'=>\Yii::$app->params['emision']])
        	->orderBy([
            	    'hora_inicio' => 'SHORT_ASC',
        	]);
    	    if($fechas[0]->horario_de_registro == false){
    	        $horariosquery->andWhere(['hora_de_registro' => false ]);
    	    }
    	    $horariodia1 = $horariosquery->all();
    	}else{
    	    $eventosdia1 = [];
    	    $horariodia1 = [];
    	}
    //**** Eventos dia 2 ****//
	if(isset($fechas[1])){
    	    $eventosdia2 = $this-> getDateEvents(
    		$fechas[1]->fecha // Manda la fecha del segundo dia
    		, $fechas[1]->horario_de_registro //Manda si el dia tiene horario para registro de asistentes
    		);
    	    $horariosquery = HorariosPorEmision::find()
        	->where(['emision'=>\Yii::$app->params['emision']])
        	->orderBy([
            	    'hora_inicio' => 'SHORT_ASC',
        	]);
    	    if($fechas[1]->horario_de_registro == false){
    		$horariosquery->andWhere(['hora_de_registro' => false ]);
    	    }
    	    $horariodia2 = $horariosquery->all();
    	}else{
        	$eventosdia2 = [];
		$horariodia2 = [];
    	}
    //**** Eventos dia 3 ****//
	if(isset($fechas[2])){
    	    $eventosdia3 = $this-> getDateEvents(
    		$fechas[2]->fecha // Manda la fecha de tercer dia
    		, $fechas[2]->horario_de_registro //Manda si el dia tiene horario para registro de asistentes
    	    );
    	    $horariosquery = HorariosPorEmision::find()
        	->where(['emision'=>\Yii::$app->params['emision']])
        	->orderBy([
            	    'hora_inicio' => 'SHORT_ASC',
        	]);
    	    if($fechas[2]->horario_de_registro == false){
    		$horariosquery->andWhere(['hora_de_registro' => false ]);
    	    }
//    	    if(isset(\Yii::$app->user->identity->name) && \Yii::$app->user->identity->name == "Admin")
//    		throw new \yii\base\Exception(print_r($eventosdia3));
    	    $horariodia3 = $horariosquery->all();
    	}else{
    	    $eventosdia3 = [];
    	    $horariodia3 = [];
    	}
        return $this->render($template,[
    	    'fechas' => $fechas,
    	    'locaciones'=> $locaciones,
    	    'horariodia1' => $horariodia1,
    	    'eventosdia1' => $eventosdia1,
    	    'horariodia2' => $horariodia2,
    	    'eventosdia2' => $eventosdia2,
    	    'horariodia3' => $horariodia3,
    	    'eventosdia3' => $eventosdia3,
    	]);
    }
    private function getDateEvents($date, $tiene_horario_de_registro=false)
    {
        $query_horarios = HorariosPorEmision::find()
    	    ->where(['emision'=>\Yii::$app->params['emision']])
    	    ->orderBy([
        	'hora_inicio' => 'SHORT_ASC',
    	    ]);
    	if($tiene_horario_de_registro == false){ // Filtra solo horarios que no son de registro
    	    $query_horarios->andWhere(['hora_de_registro' => false ]);
    	}
    	$horarios = $query_horarios->all();
        $locaciones = Locaciones::find()
    	    ->where(['emision'=>\Yii::$app->params['emision']])
    	    ->all();
	$data = array();
        for($i=0; $i<=50; $i++)
            $data[$i] = array();
        $i = 0;
        foreach ($horarios as $horario) 
        {
            $j = 0;
            //$data1[$i][$j] = $horario;
            foreach ($locaciones as $locacion) 
            {
                $data[$i][$j] = Fechas::getEventByHourLocacion($horario->id, $locacion->id, $date);
                $j++;
            }
            $i++;
        }
        // se unifican los eventos iguales y contiguos en la misma columna
        if (count($horarios) > 0)
        for($j = 0; $j < 19; $j++)
        {
            for($i = 0; $i < 49; $i++)
            {
                $count = 1;
                
                while ((isset($data[$i][$j]) && isset($data[$i+$count][$j])) && ($data[$i][$j]->evento->id == $data[$i+$count][$j]->evento->id) ) {
                    $data[$i+$count][$j] = null;
                    $count++;
                }
                if (isset($data[$i][$j])) $data[$i][$j]->rowspan = $count;
                $i = $i + $count-1;
                
            }
        }
        return $data;        
    }

    public function actionDetalle($id)
    {

        $model = Eventos::findOne($id);
        return $this->renderAjax('detalle',[
            'model' => $model,
        ]);

    }
    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->identity->name) and \Yii::$app->user->identity->name!=null)?\Yii::$app->user->identity->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
