<?php
/*
*** Controlador para registrar usuarios a eventos
*** utilizado solo mediante llamadas AJAX
*/
namespace app\controllers;

use Yii;
use app\models\Asistencia;
use app\models\Fechas;
use app\models\Eventos;
use app\models\Logs;
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use yii\web\ConflictHttpException;
use yii\web\NotFounfHttpException;

class AsistenciaController extends \yii\web\Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(['/public']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
	    // No tiene vista, solo usado mediante ajax
            $this->redirect(Url::to(['/info']));           
    }

    public function actionActualizar()
    {
        if(!\Yii::$app->user->identity)
            return Json::encode(['success'=>false]);
        if(\Yii::$app->request->post('id_evento') == null)
            return Json::encode(['success'=>false]);
        $id_user = \Yii::$app->user->identity->id;
        $id_evento = \Yii::$app->request->post('id_evento');
        $asistencia = new Asistencia();
        $asistencia->id_user = $id_user;
        $asistencia->id_evento = $id_evento;
        if(!$asistencia->save())
            return Json::encode(['success'=>false]);
        else
            return Json::encode(['success'=>true]);
    }
    
    /**
     * Finds the Asistencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Asistencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Asistencia::findOne($id)) !== null) {
            return $model;
        } else if (($model = Asistencia::findOne($id)) == null) {
            return new Asistencia;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'La página solicitada no existe.'));
        }
    }

    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->identity->name) and \Yii::$app->user->identity->name!=null)?\Yii::$app->user->identity->name:'No user';
	$log->logtime = date('Y-m-d H:i:s');
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
