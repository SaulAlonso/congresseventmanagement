<?php

namespace app\controllers;
use app\models\Logs;
use Yii;

class LangController extends \yii\web\Controller
{
	public function init()
	{
		\Yii::$app->language = \Yii::$app->session['lang'];
	}

    public function actionIndex()
    {
        return $this->render('index',['session'=>\Yii::$app->session]);
    }
    public function actionChangeLang($lang)
    {
        $session = \Yii::$app->session;
        $session->set('lang', $lang);
        \Yii::$app->language =  \Yii::$app->session['lang'];
        //return $this->redirect(\Yii::$app->request->referrer);
        return $this->goBack((!empty(\Yii::$app->request->referrer) ? \Yii::$app->request->referrer : null));
    }

    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->name) and \Yii::$app->user->name!=null)?\Yii::$app->user->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
?>