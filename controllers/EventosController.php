<?php

namespace app\controllers;

use Yii;
use app\models\Eventos;
use app\models\EventosSearch;
use app\models\Tipos;
use app\models\Fechas;
use app\models\Locaciones;
use app\models\Idiomas;
use app\models\Logs;
use app\models\Users;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\UploadedFile;

/**
 * EventosController implements the CRUD actions for Eventos model.
 */
class EventosController extends Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Eventos models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $searchModel = new EventosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Eventos model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Eventos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Eventos();
        $tipos_de_eventos = Tipos::find()->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $locaciones = Locaciones::find()->where(['emision'=>\Yii::$app->params['emision']])->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $idiomas = Idiomas::find()->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $users = Users::find()->orderBy(['name'=>'SHORT_ASC'])->all();
        if ($model->load(Yii::$app->request->post()))
        {
            $fileupload = UploadedFile::getInstance($model, 'foto');
            if(!empty($fileupload)){
                $path = 'img/wsi/'.$fileupload->baseName . '.' . $fileupload->extension;
              $fileupload->saveAs($path);
              $model->foto = $path;
            }
        }
        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'tipos_de_eventos' => $tipos_de_eventos,
                'idiomas' => $idiomas,
                'locaciones' => $locaciones,
                'users' => $users
            ]);
        }
    }

    protected function uploadImage($model)
    {
        $fileupload = UploadedFile::getInstance($model, 'foto');
        if(!empty($fileupload)){
            $path = 'img/wsi/'.$fileupload->baseName . '.' . $fileupload->extension;
            $fileupload->saveAs($path);
            $model->foto = $path;
        }
    }

    /**
     * Updates an existing Eventos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $tipos_de_eventos = Tipos::find()->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $locaciones = Locaciones::find()->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $idiomas = Idiomas::find()->orderBy(['nombre' => 'SHORT_ASC'])->all();
        $users = Users::find()->orderBy(['name'=>'SHORT_ASC'])->all();
        $current_image = $model->foto;
        if ($model->load(Yii::$app->request->post()))
        {
            $fileupload = UploadedFile::getInstance($model, 'foto');
            if(!empty($fileupload)){
                $path = 'img/wsi/'.$fileupload->baseName . '.' . $fileupload->extension;
              $fileupload->saveAs($path);
              $model->foto = $path;
            }else
            {
                $model->foto = $current_image;
            }

            if ($model->save())
                return $this->redirect(['view', 'id' => $model->id]);
            else
                return $this->render('update', [
                    'model' => $model,
                    'tipos_de_eventos' => $tipos_de_eventos,
                    'idiomas' => $idiomas,
                    'locaciones' => $locaciones,
                    'users' => $users
                ]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'tipos_de_eventos' => $tipos_de_eventos,
                'idiomas' => $idiomas,
                'locaciones' => $locaciones,
                'users' => $users
            ]);
        }
    }

    /**
     * Deletes an existing Eventos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        $this->findModel($id)->delete();
        Fechas::deleteAll('id_evento=:id_evento',[':id_evento'=>$id]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Eventos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Eventos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Eventos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'La página solicitada no existe.'));
        }
    }

    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->name) and \Yii::$app->user->name!=null)?\Yii::$app->user->name:'No user';
        $log->logtime = date('Y-m-d H:i:d');
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
