<?php

namespace app\controllers;

use Yii;
use app\models\Idiomas;
use app\models\IdiomasSearch;
use app\models\Logs;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IdiomasController implements the CRUD actions for Idiomas model.
 */
class IdiomasController extends Controller
{
    public function init()
    {
        if(!isset(\Yii::$app->user->identity))
            $this->redirect(Url::to(\Yii::$app->params['cimpsPage']));
        \Yii::$app->language = \Yii::$app->session['lang'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Idiomas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IdiomasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Idiomas model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Idiomas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Idiomas();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Idiomas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Idiomas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Idiomas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Idiomas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Idiomas::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'La página solicitada no existe.'));
        }
    }

    public function beforeAction($action)
    {
        $log = new Logs();
        $log->username = (isset(\Yii::$app->user->name) and \Yii::$app->user->name!=null)?\Yii::$app->user->name:'No user';
        $log->ipaddress = $_SERVER['REMOTE_ADDR'];
        $log->logtime = date('Y-m-d H:m:s');
        $log->controller = ($action->controller->id!=null)?$action->controller->id:'';
        $log->action = ($action->id!=null)?$action->id:'';
        $log->details = (isset($action->logMessage) and $action->logMessage!=null)?$action->logMessage:'';
        $log->save();
        return true;
    }
}
