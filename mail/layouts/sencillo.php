<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content Array main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php 
	$this->beginBody(); 
    echo $user->name.\Yii::t('app', ' gracias por tu participación en el Congreso Internacional de Mejora de Procesos de Software 2017').'<br>';
    echo \Yii::t('app', 'Para más información consulta la la página ').'<a href="http://cimps.cimat.mx" target="_blank">CIMPS 2017</a><br>';
    echo \Yii::t('app', 'Se ha adjuntado a este correo un documento PDF con los talleres que has seleccionado, recuerda llevarlo impreso o electronico durante tu asistencia al congreso.').'<br>';
    echo \Yii::t('app', 'Te esperamos!').'<br>';
    $this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>