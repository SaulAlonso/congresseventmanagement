    		    		    		    		    		<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title>CIMPS <?=Date('Y')?></title>
</head>
<body>
<?php 
    $this->beginBody(); 
echo \Yii::t('app', '<p style="margin: 3px; font-weight: bold;">Estimado asistente.</p>').'<br>';

echo \Yii::t('app', '<p style="margin: 3px;">
Con la finalidad de hacer su experiencia en CIMPS 2018 aún más práctica al momento de registrarse y acceder a los distintos talleres y ponencias, le informamos que deberá descargar su código QR, que será utilizado como Identificación Personal. Su código QR se encuentra en la sección Inscripción a conferencias, sesiones y talleres, de su perfil en la página CIMPS 2018: http://cimps.cimat.mx/registro/index.php/auth</p>').'<br>';
echo \Yii::t('app', '<p style="margin: 3px;">
Nota: El registro comenzará el día miércoles 17 a las 9:00 AM. </p>').'<br>';
echo \Yii::t('app', '<p style="margin: 3px; font-weight: bold;">Dear attendee:</p>').'<br>';

echo \Yii::t('app', '<p style="margin: 3px;">
In order to make your experience at CIMPS 2018 even more practical when registering and accessing the different workshops and presentations, we inform you that you must download your QR code, which will be used as a Personal Identification. Your QR code can be found in the section Inscripción a conferencias, sesiones y talleres, of your profile on the CIMPS 2018 page: http://cimps.cimat.mx/registro/index.php/auth</p>').'<br>';
echo \Yii::t('app', '<p style="margin: 3px;">
Note: Registration will begin on Wednesday, October 17th at 9:00 AM.</p>').'<br>';
   $this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
    		    		    		    		    		    		