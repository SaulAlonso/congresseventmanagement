<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title>CIMPS <?=Date('Y')?></title>
</head>
<body>
<?php 
	$this->beginBody(); 
	echo '================= INVITATION<br>';
	echo 'Please consider to contribute to and/or forward to the appropriate groups the following opportunity to submit and publish original scientific results to this Special Issue.<br>';
	echo 'DATES:<br>';
	echo 'Paper Sudmission: 1 February 2019<br>';
	echo '=================<br>';
	echo '<br>';
	echo 'This Special Issue seeks research articles which include original, pertinent and relevant contributions from academy and industry to cover diverse topics reporting state-of-the art advances, case studies, approaches and techniques, and industry experience. In addition to direct submission, in this Special Issue, we also invite the best articles from the International Conference on Software Process Improvement (CIMPS) 2018 (cimps.cimat.mx), which will take place from October 17 to 19, in Guadalajara, Jalisco, Mexico.<br>';
	echo '<br>';
	echo 'Link: http://digital-library.theiet.org/content/journals/iet-sen/info/spl-issues;jsessionid=e87sgem9vl9f.x-iet-live-01<br>';
	echo 'pdf download: http://digital-library.theiet.org/files/IET_SEN_CFP_SEASOI.pdf<br>';
	echo '<br>';
	echo 'Topics of interest include, but are not limited to:<br>';
	echo '<ul>';
	echo '<li> Software improvement in emergent market</li>';
	echo '<li> Process improvement in very small, small, medium and large organisations</li>';
	echo '<li> Implementation of agile methods across different models and standards</li>';
	echo '<li> Software architectures trends</li>';
	echo '<li> Data analysis for organizational decision making</li>';
	echo '<li> Implementation of ontologies to develop solutions</li>';
	echo '<li> Improvements in software organisations based on human and cultural factors</li>';
	echo '<li> Software engineering applications in non-software domains (mining, automotive, aerospace, business, health care, manufacturing, etc.)</li>';
	echo '<li> Improvements in security information</li>';
	echo '<li> Software Quality Assurance trends</li>';
	echo '<li> Implementation of models or standards</li>';
	echo '</ul><br>';
	echo 'Best regards<br>';
	echo 'CIMPS 2018<br>';
    $this->endBody();
?>
</body>
</html>
<?php $this->endPage() ?>
