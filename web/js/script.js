$(document).ready(function(){
    var flag = false; // set to enable/disable modal window alerts
    if (flag)
    {
    	$('#modal').modal('show');
    	//setTimeout(function(){$('.modal.in').modal('hide');}, 6000); // close it by timer
    }

    // to call regist controller action
    $('.register').on("click",function(){
	var response = confirm("¿Confirma el registro al evento?");
	if (response)
	{
    	    var div_called = $(this);
    	    var id_evento = div_called.data("event");
    	    div_called.html('<i class="fa fa-spinner fa-spin fa-3x"></i>');
    	    $.ajax({
        	url : "index.php?r=asistencia%2Factualizar",
    		method : "post",
        	data : {id_evento: id_evento},
        	success: function(data) {
            	    var parsed_data = JSON.parse(data);
            	    if (parsed_data.success)
	            	div_called.parent().html('<i class="fa fa-check  fa-3x" aria-hidden="true"></i>');
	            else
	            	div_called.parent().html('<i class="fa fa-ban  fa-3x" aria-hidden="true"></i>');
	        }
    	    });
	}
        return false;
    });
});