$(document).ready(function(){
    // to test send mail
    $('#mailTestButton').on("click", function() {
	var correo = $("#correoTest").val();
	if (correo==""){
	    alert("Ingrese un correo electrónico.");
	     return false;
	}
	var answer = confirm("Confirma el envio de correo de prueba a "+correo+"?");
	if (answer)
	    $("#loading_gif_test").css('display','block');
    	    $.ajax({
        	url : "index.php?r=mail/test",
    		method : "post",
        	data : {correo: correo},
	        success: function(data) {
		    $("#loading_gif_test").css('display','none');
            	    alert(data);
        	},
        	failure: function(result){
		    $("#loading_gif_test").css('display','none');
        	    alert(result);
        	},
    	    });
    });
    // to send massive mail
    $('#mailMassiveButton').on("click",function(){
	var subject = $("#masiveMailSubject").val();
	if (subject==""){
	    alert("Indica el asunto del correo.");
	    return false;
	}
	var answer = confirm("Confirma el envio masivo de correos?");
	if (answer){
	var answer = confirm("Ya lo autorizó el doc Jezreel?");
	if (answer){
	var answer = confirm("Ya le avisaste a Facundo para que habilite la excepción y no quede bloqueada la cuenta?");
	if (answer){
	var answer = confirm("Ya revisaste la tabla de correos en la base de datos, que sean los correctos?");
	if (answer){
    	var answer = confirm("Ya revisaste la plantilla, que esté actualizada?");
	if (answer){
	$("#loading_gif_masivo").css('display','block');
	$.ajax({
            url : "index.php?r=mail/masivo",
            method : "post",
            data : {authorized: true, subject: subject},
            success: function(data) {
    		$("#loading_gif_masivo").css('display','none');
        	alert(data);
	    },
            failure: function(result){
		$("#loading_gif_masivo").css('display','none');
        	alert(result);
            },
        });
        }}}}}
    });
    // to update masive mail template
    $('#updateTemplate').on("click", function() {
	var plantilla = $("#masiveMailTemplate").val();
	if (plantilla==""){
	    alert("No puedes dejar sin texto la plantilla.");
	     return false;
	}
	var answer = confirm("Confirma actualizar el contenido de la plantilla?");
	if (answer)
	    $("#loading_gif_masivo").css('display','block');
    	    $.ajax({
        	url : "index.php?r=mail/updatetemplate",
    		method : "post",
        	data : {plantilla: plantilla, authorized: true},
	        success: function(data) {
		    $("#loading_gif_masivo").css('display','none');
            	    alert(data);
        	},
        	failure: function(result){
		    $("#loading_gif_masivo").css('display','none');
        	    alert(result);
        	},
    	    });
    });
});