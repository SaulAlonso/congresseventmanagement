$(document).ready(function(){
    $('#certificates-mail').on("click",function(){
	var result = confirm('Se utilizara el formulario de busqueda para la seleccion de usuarios, confirma el envio de las constancias?');
	if(result)
	{
            $('#w0').append("<input type='hidden' name='send_mail' value='true' />");    
            $( "#w0" ).submit();
	}
        return false;
    });
    $('#w1 .glyphicon-envelope').on("click", function(){
	var conf = confirm("Confirma enviar el certificado a este usuario?");
	if(conf)
	{
    	    var a = $(this);
    	    var href = a.attr("href");
    	    $.ajax({
    		url : href,
    		success: function(data) {
            	    var parsed_data = JSON.parse(data);
	    	    $('#w0').submit();
		}
    	    });
    	}
    	return false;
    });
});
