$(function() {
    $(document).on('click', '.show-modal-button', function(){
        if($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modal_content')
                .load($(this).attr('value'));
            document.getElementById('modal_header_title')
                .innerHTML = '<h2>' + $(this).attr('title') + '</h2>';
        } else {
            $('#modal').modal('show')
                .find('#modal_content')
                .load($(this).attr('value'));
            document.getElementById('modal_header_title')
                .innerHTML = '<h2>' + $(this).attr('title') + '</h2>';
        }
    });

    $('#modal').on('hide.bs.modal', function(){
        $('#modal').find('#modal_content').html(
         "<div style='text-align:center'><img src='img/ajax-loader.gif'></div>"
         );
    });

})(jQuery);
