<?php

return [
    'emision' => 2018,
    'logoEmision' => 'img/Recurso16.png',//'img/LogoCIMPS17-4.png',
    'adminEmail' => 'conferencecimps@cimat.mx',
    'languages'=>[
        'es' => 'Español',
        'en' => 'English',
    ],
    'cimpsPage'=> 'http://cimps.cimat.mx/',
    'key' => 'GiUt@*q564h85m&',
    'sessionTimeoutSeconds'=>300,  //timeout value in seconds
    'cron_correo_masivo'=>'0 1 * * wget -S "http://srcimps.cimat/cimatcimps/web/index.php?r=mail%2Fmasivo"'// ejecuta acción en controlador
];
