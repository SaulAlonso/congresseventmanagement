<?php

namespace yii\jui;

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $fechas yii\data\ActiveDataProvider */
/* @var $asistencia yii\data\ActiveDataProvider */
/* @var $result yii\data\ActiveDataProvider */

?>
<script type="text/javascript" src="js/script.js"></script>
<div class="div-sections">
  <img class="img cimps-logo" src="img/LogoCIMPS17-4.png"/>
  <img class="img cimat-logo" src="img/cimat-logo.png"/>
  <img class="img pnpc-logo" src="img/pnpc.png"/>
</div>
<br>
<div class="div-sections">
  <h1><?=\Yii::t('app', 'Programa General')?></h1>
  <br>
  <?=Tabs::widget([
      'items' => [
          [
              'label' => '18 Oct 2017',
              'options' => ['id' => 'tab1'],
              'content' => $this->render('_tab1', 
              	    [
                    'eventos' => $eventos
                    ]
                ),
          ],
          [
              'label' => '19 Oct 2017',
              'options' => ['id' => 'tab2'],
              'content' => $this->render('_tab2', 
              	    [
                    'eventos' => $eventos
                    ]
                ),
          ],
          [
              'label' => '20 Oct 2017',
              'options' => ['id' => 'tab3'],
              'content' => $this->render('_tab3', 
              	    [
                    'eventos' => $eventos
                    ]
                ),
          ],
      ],
  ]);
 ?>
 </div>
